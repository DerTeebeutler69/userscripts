const WebpackUserscript = require('webpack-userscript')
const path = require('path')
const isDevelopment = process.env.NODE_ENV === 'development'

console.log("devmode", isDevelopment)
// configure your script stuff here
const pathOut = path.resolve(__dirname, '..', '..', 'dist')
const entryPoint = './gimmeCharts.js' // this script should require all other files that are needed, each file should export something
const scriptName = path.basename(__dirname) // by default use folder name
const tamperMonkeyHeader = {
  name: scriptName,
  version: "1.4.0",
  description: "collects your messages and shows you fancy charts",
  author: "joghurtrucksack",
  require: [
    "https://cdn.jsdelivr.net/npm/chartjs-plugin-autocolors",
    "https://cdn.jsdelivr.net/npm/chart.js@3.7.1/dist/chart.min.js"
  ],
  include: "/https:\\/\\/(www.|)pr0game\\.com\\/game\\.php.*/",
  grant: [
    "GM_setValue",
    "GM_getValue"
  ]
}
// stop here

// add buildnumber to version to make it unique for hot reload via proxy script
if (isDevelopment) {
  tamperMonkeyHeader.version += '-build.[buildTime]'
}

module.exports = {
  mode: isDevelopment ? 'development' : 'production',
  entry: entryPoint,
  output: {
    path: pathOut,
    filename: `${scriptName}.user.js`
  },
  devtool: 'source-map',
  plugins: [
    new WebpackUserscript({
      updateBaseUrl: 'https://codeberg.org/pr0game/userscripts/raw/branch/master/',
      headers: tamperMonkeyHeader,
      metajs: false,
      ssri: true,
      proxyScript: {
        baseUrl: `file://${pathOut}`,
        filename: '[basename].proxy.user.js',
        enable: isDevelopment
      }
    })
  ]
}