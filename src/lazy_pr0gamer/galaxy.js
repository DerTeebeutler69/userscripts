import {GM_getValue, GM_setValue} from "./general";

export function debris_text() {
    if (!location.href.includes("page=galaxy")) {
        return;
    }
    const tfmin = GM_getValue("galaxy_tf_min", 0)
    for (let db of document.querySelectorAll("img[src *='debris']")) {
        let text = db.parentNode.getAttribute("data-tooltip-content")
        let [met, krist] = text.match(/(\d+\.)*\d+<\//g)
        met = parseInt(met.replace("</", "").replaceAll(".", ""))
        krist = parseInt(krist.replace("</", "").replaceAll(".", ""))
        let dnode = document.createElement("div")
        dnode.innerHTML = `M: ${shortly_number(met)}<br>K: ${shortly_number(krist)}`
        if (met + krist < tfmin) {
            continue
        }
        db.parentNode.appendChild(dnode)
    }
    let hack = document.getElementsByClassName("hack")[0]

    hack.innerHTML = `TF min:<input type="number" id="tf_min" value="${tfmin}">`
    document.getElementById("tf_min").addEventListener("change", set_debris_limit);

}

function set_debris_limit() {
    let nbr = parseInt(document.getElementById("tf_min").value)
    GM_setValue("galaxy_tf_min", nbr)
}


