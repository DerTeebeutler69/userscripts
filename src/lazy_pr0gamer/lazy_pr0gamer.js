/* globals dataFlyTime, calculateTransportCapacity, shortly_number, noShips, calculateRatios, serverTime */
'use strict';
import {GM_setValue, GM_getValue, get_sortet_planet_list} from "./general"
import {marketstuff} from "./market";
import {show_phalanx_exact} from "./phalanx";
import {showexact_time} from "./overview";
import {
    calcfull,
    calctime,
    saveres,
    set_planets,
    show_prodph,
    update_current_res,
    update_production
} from "./resources";
import {
    add_exp_eventlisteners,
    add_expo_buttons,
    set_arrival_time,
    set_max_exp_score,
    show_arrival,
    show_exp_value
} from "./fleet";
import {
    commander_building_reader, commander_building_table,
    commander_def_reader, commander_def_table, commander_fleet_table,
    commander_res_table,
    get_imperiom_stuff,
    save_planet_fleet
} from "./imperium";
import {expo_table, selectmesages} from "./expos";
import {custom_fleet_table, loadcustomfleets} from "./fleet_custom";

function gen_fieldset(id, func, name) {
    const fields = GM_getValue("menu_vis", {})
    return `<fieldset ><legend style="float:right"><a  id="${id}" href="#" onclick="var spoilernode = this.parentNode.parentNode.lastChild; if (spoilernode.style.display === 'block') spoilernode.style.display='none'; else spoilernode.style.display = 'block'; return false;">${name}</a></legend><div style="display: ${fields[id]}" class="tscroll">${func()}</div></fieldset>`
}

function addstuff(side_nodes) {
    const planets_selected = GM_getValue("select", {})
    var psel_str = ""
    for (let p of get_sortet_planet_list(Object.keys(planets_selected))) {
        let checked = ""
        if (planets_selected[p] === 1) {
            checked = "checked"
        }
        psel_str += '<input type="checkbox" id="' + p + '"  ' + checked + ' >[' + p + '<span id="full_' + p + '"></span><br>'
    }

    var div = document.createElement("div");
    div.style.cssText = ' grid-row: 2; grid-column: 1;    overflow-x: hidden;width: 20vw;'
    var ct = document.createElement("center");
    ct.id = "atain"
    div.className = "no-mobile";
    div.appendChild(ct)
    document.getElementsByClassName("wrapper")[0].appendChild(div)
    var li = document.createElement("li")
    li.style.cssText = "    overflow: hidden;    font-size: 12px;    background: #2a2e31;"
    li.id = "disc2"
    li.innerHTML = `
M:<input placeholder="met" id="t_met" type="number" step="1000" ><br>
K:<input placeholder="krist" id="t_krist" type="number" step="1000"><br>
D:<input placeholder="deut" id="t_deut" type="number" step="1000">
<br>
current:<br>M:
<span id="c_met"></span><br>K:<span id="c_krist"></span><br>D:<span id="c_deut"></span>
<br>
prod:
<span id="p_met"></span>,<span id="p_krist"></span>,<span id="p_deut"></span>
<br>
time:
<span id="time_til_got"></span>
<br>

` + psel_str

    document.getElementById("menu").appendChild(li)
    var ihtml = ""
    for (let node of side_nodes) {
        ihtml += gen_fieldset(node.id, node.table, node.name)
    }

    document.getElementById("atain").innerHTML = ihtml;
    document.getElementById("t_met").addEventListener("change", saveres);
    document.getElementById("t_krist").addEventListener("change", saveres);
    document.getElementById("t_deut").addEventListener("change", saveres);
    for (let node of side_nodes) {
        document.getElementById(node.id).addEventListener("click", change_menu);
        if (node.func !== null) {
            node.func();
        }
    }

    const resstatus = GM_getValue("res_input", [0, 0, 0])
    document.getElementById("t_met").value = resstatus[0]
    document.getElementById("t_krist").value = resstatus[1]
    document.getElementById("t_deut").value = resstatus[2]
    let a = document.createElement("div")
    let b = document.createElement("div")
    let c = document.createElement("div")
    a.style.cssText = 'font-size: 11px;color:#0F0'
    b.style.cssText = 'font-size: 11px;color:0F0'
    c.style.cssText = 'font-size: 11px;color:0F0'
    a.innerHTML = "<span style='color:yellow' id='met_top'></span> <span id='met_top_p' style='color:#0F0'></span>"
    b.innerHTML = "<span style='color:yellow' id='krist_top'></span> <span id='krist_top_p' style='color:#0F0'></span>"
    c.innerHTML = "<span style='color:yellow' id='deut_top'></span> <span id='deut_top_p' style='color:#0F0'></span>"

    document.getElementsByClassName("no-mobile")[4].appendChild(a)
    document.getElementsByClassName("no-mobile")[6].appendChild(b)
    document.getElementsByClassName("no-mobile")[8].appendChild(c)
    for (let n of document.getElementById("disc2").querySelectorAll('input[type="checkbox"]')) {
        n.addEventListener('change', planet_select_change);
    }
}

function change_menu() {
    const cm = GM_getValue("menu_vis", {})
    let id = this.id
    if (cm[id] === "none") {
        cm[id] = "block"
    } else {
        cm[id] = "none"
    }
    GM_setValue("menu_vis", cm);
}


function planet_select_change() {
    const planets_selected = GM_getValue("select", {})
    planets_selected[this.id] = Math.abs(planets_selected[this.id] - 1)
    GM_setValue("select", planets_selected)
    show_prodph();
    calctime();
}

function remove_old_planets() {
    let planets = document.getElementById("planetSelector")
    if (planets == null) {
        return;
    }
    let pids = {}
    for (let p of planets.children) {
        let pname = p.innerText.split("[")[1]
        if (p.innerText.includes("(")) {
            pname = pname + "M"
        }
        pids[pname] = 1
    }
    const gmnames = ["commander_building",
        "res",
        "prod",
        "commander_fleet",
        "commander_def"]
    for (let gmv of gmnames) {
        let mvx = GM_getValue(gmv, {})
        let rmv = []
        for (let k in mvx) {
            if (pids[k] !== 1) {
                rmv.push(k)
            }
        }
        for (let k of rmv) {
            delete mvx[k];
        }
        GM_setValue(gmv, mvx)
    }
}

remove_old_planets();


function add_talbe_scroll() {

    var style = document.createElement('style');
    style.type = 'text/css';
    if (style.styleSheet) {
        style.styleSheet.cssText = `.tscroll {
  width: 100%;
  overflow-x: auto;
}

.tscroll table *:first-child {
  position: sticky;
  left: 0;
}
`;
    } else {
        style.appendChild(document.createTextNode(`.tscroll {
    width: 20vw;
  overflow-x: auto;
}

.tscroll table *:first-child {
  position: sticky;
  left: 0;
}
`));
    }
    document.getElementsByTagName('head')[0].appendChild(style);


}

function set_importance_fleet() {
    if (!window.location.href.includes("page=fleetStep2")) {
        return
    }
    const priority = GM_getValue("fleet_prio_list", ["radio_2", "radio_6", "radio_1", "radio_4", "radio_3", "radio_5", "radio_17"])
    for (let prio of priority) {
        let btn = document.getElementById(prio)
        if (btn) {
            if (btn.checked) {
                return
            }
        }
    }

    for (let prio of priority) {
        let btn = document.getElementById(prio)
        if (btn) {
            btn.checked = true
            break
        }

    }
}

function priotable_table() {
    const priority = GM_getValue("fleet_prio_list", ["radio_2", "radio_6", "radio_1", "radio_4", "radio_3", "radio_5", "radio_17"])
    const btn_to_action = {
        "radio_2": "AKS-angriff",
        "radio_3": "transport",
        "radio_6": "Spio",
        "radio_17": "transfer",
        "radio_1": "angreifen",
        "radio_5": "halten",
        "radio_4": "stationieren"
    }

    var html = "<table id='event_prio_list'>       <tr>   <th>Event</th><th>Priority</th>       </tr>"
    for (let action in priority) {
        html += `<tr id="${priority[action]}" draggable="true" ondragstart="priodrag()"  ondragover="dragover()"> <td >${btn_to_action[priority[action]]}</td><td><input id="${priority[action]}" style="    width: 50px;" value="${action}"></td></tr> `
    }

    html += "</table>"
    return html


}


function gen_prio_list() {
    let inpts = document.getElementById("event_prio_list").querySelectorAll("input")
    let aryobj = []
    for (let event of inpts) {
        aryobj.push({id: event.id, prio: event.value})

    }

    aryobj.sort(function (a, b) {
        return a.prio - b.prio
    });
    let nprio = []
    for (let v of aryobj) {
        nprio.push(v.id)
    }
    GM_setValue("fleet_prio_list", nprio);
}

function priotable_interactive() {
    let inpts = document.getElementById("event_prio_list").querySelectorAll("input")
    for (let ele of inpts) {
        ele.addEventListener("change", gen_prio_list);

    }


}

window.row;
var row = event.target;

window.priodrag = function () {
    row = event.target;
}
window.dragover = function () {
    var e = event;
    e.preventDefault();

    let children = Array.from(e.target.parentNode.parentNode.children);

    if (children.indexOf(e.target.parentNode) > children.indexOf(row))
        e.target.parentNode.after(row);
    else
        e.target.parentNode.before(row);
    gen_prio_list_move();
}

function gen_prio_list_move() {

    let inpts = document.getElementById("event_prio_list").querySelectorAll("tr")
    let aryobj = []
    for (let event of inpts) {
        if (event.id === "") {
            continue
        }
        aryobj.push(event.id)

    }
    GM_setValue("fleet_prio_list", aryobj);


}


let modules = [{id: "custom_commander_res", table: commander_res_table, name: "Commander Res", func: null},
    {id: "custom_commander_building", table: commander_building_table, name: "Commander Build", func: null},
    {id: "custom_commander_fleet", table: commander_fleet_table, name: "Commander Fleet", func: null},
    {id: "custom_commander_def", table: commander_def_table, name: "Commander Def", func: null},
    {id: "expo_menu", table: expo_table, name: "Expo statistik", func: null},
    {id: "custom_fleet_menu", table: custom_fleet_table, name: "Custom Fleet", func: loadcustomfleets},
    {id: "priotable", table: priotable_table, name: "Fleet prio", func: priotable_interactive}]

show_phalanx_exact()
set_importance_fleet();

marketstuff();

add_talbe_scroll();


//imperium

get_imperiom_stuff();
commander_def_reader();
commander_building_reader();
save_planet_fleet();


selectmesages(); //expo

//fleet
add_expo_buttons();
add_exp_eventlisteners();
show_exp_value();
set_max_exp_score();
set_arrival_time();
show_arrival();

//res
addstuff(modules);

set_planets();
update_current_res();
update_production();
show_prodph();
calcfull();
calctime();
setInterval(calcfull, 1000);


//overview
showexact_time();





