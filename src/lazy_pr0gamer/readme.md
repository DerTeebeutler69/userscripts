# raidstats

## Development

For development you can install a proxy userscript that automatically loads the latest version of your userscript from a local webserver.
To do this you need to configure tampermonkey to check for `@require`d scripts every time

1. Set tampermonkey settings to `advanced` 
1. Set Externals / Update Interval to `always`
> You need to reload your page twice for the new script to show (new version is downloaded after first page render)

To get your script into local hotreload development mode, simply do

```sh
cd src/raidstats
npm run serve # will start webpack with watchmode to build on filechange and open local development server on port :8080
# go to http://localhost:8080 and install the raidstats.proxy.user.js script in tampermonkey
```

## Delivery

The version of the script is defined in the `webpack.config.js` within the `tamperMonkeyHeader` variable.

To build a production version of the script run

```sh
npm run build
```

the output will be placed into `dist/` folder.
From there copy it to the root directory of this repo.
