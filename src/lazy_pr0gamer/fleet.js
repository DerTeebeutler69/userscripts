import {GM_getValue, GM_setValue, pad} from "./general";

export function set_arrival_time() {

    if (window.location.href.includes("page=fleetStep1")) {
        let xf = document.getElementsByClassName("table519")
        xf[xf.length - 1].getElementsByTagName("input")[0].addEventListener("click", fleet_set_target);
    }
}

function fleet_set_target() {
    GM_setValue("fleet_arrival", dataFlyTime);
    const t_koords = document.getElementById("galaxy").value + ":" + document.getElementById("system").value + ":" + document.getElementById("planet").value

    GM_setValue("fleet_t_coords", [document.getElementById("planet").value, t_koords]);
}

function get_expos_on_koord(koord) {

    const expos = GM_getValue("fleet_expos", {})
    if (!(koord in expos)) {
        return 0;
    }
    let nary = []
    const ctime = Date.now()
    for (let time of expos[koord]) {
        if (ctime - time < 86400000) {
            nary.push(time)
        }

    }
    expos[koord] = nary
    GM_setValue("fleet_expos", expos);
    return nary.length

}

function fleet_Expo_add() {
    const expos = GM_getValue("fleet_expos", {})
    const target = GM_getValue("fleet_t_coords", ["-1", "-1"])[1]
    if (!(target in expos)) {
        expos[target] = [];
    }
    expos[target].push(Date.now())
    GM_setValue("fleet_expos", expos);
}

export function show_arrival() {
    if (!window.location.href.includes("page=fleetStep2")) {
        return;
    }
    let tbl = document.getElementsByClassName("table519")[0]
    tbl.getElementsByTagName("table")[1].insertRow(6)
    tbl.getElementsByTagName("table")[1].getElementsByTagName("tr")[6].innerHTML = '<td class="transparent" colspan="3" id="arr_time"></td>'
    const target = GM_getValue("fleet_t_coords", ["-1", "-1"])
    tbl.getElementsByTagName("table")[1].getElementsByTagName("td")[11].innerHTML += '<br> <a href="#" id="save_Res">Ausgewählte Rohstoffe Laden</a>'
    document.getElementById("save_Res").addEventListener("click", fleet_insert_res);
    if (target[0]=="16") {
        let expoamt = get_expos_on_koord(target[1])
        tbl.getElementsByTagName("table")[1].insertRow(7)
        tbl.getElementsByTagName("table")[1].getElementsByTagName("tr")[7].innerHTML = '<td class="transparent" colspan="3">Expos:' + expoamt + '</td>'
        document.getElementsByClassName("table519")[0].getElementsByTagName("input")[document.getElementsByClassName("table519")[0].getElementsByTagName("input").length - 1].addEventListener("click", fleet_Expo_add);
    }
    window.dataFlyTime = GM_getValue("fleet_arrival", -1)
    setInterval(update_arrival, 1000)
    update_arrival();

}

function fleet_insert_res() {
    let f = GM_getValue("fleet_trans_res", [0, 0, 0]);
    document.getElementsByName("metal")[0].value = f[0]
    document.getElementsByName("crystal")[0].value = f[1]
    document.getElementsByName("deuterium")[0].value = f[2]
    calculateTransportCapacity();
}



function update_arrival() {
    const nd = new Date(new Date(serverTime).getTime() + window.dataFlyTime * 1000)
    document.getElementById("arr_time").innerText = "Arrival:   " + nd.getHours() + ":" + pad(nd.getMinutes(), 2) + ":" + pad(nd.getSeconds(), 2)
}


export function show_exp_value() {
    if (!window.location.href.includes("pr0game.com/game.php?page=fleetTable")) {
        return
    }
    if (document.getElementsByClassName("table519")[0].getElementsByTagName("tr")[0].innerText.includes(":") === false) {
        return;
    }

    let fleets = document.getElementsByClassName("table519")[0]
    let f_rows = fleets.getElementsByTagName("tr")
    let fpoints = 0

    for (let i = 2; i < f_rows.length; i++) {
        let tds = f_rows[i].getElementsByTagName("td")
        if (tds.length !== 4) {
            continue
        }
        let name = tds[0].innerText.trim()
        if (name === "Solarsatellit" || name === "Solar Satellite") {
            continue
        }
        let setamt = Number(tds[3].firstChild.value.replaceAll(".", ""))
        fpoints += exp_Values[name] * setamt
    }
    fleets.getElementsByTagName("td")[3].innerText = fpoints + " / " + GM_getValue("exp_max", -1)
}




function get_best_ship_type() {
    let bestname = ""
    let bestscore = 0;
    let bestid = -1
    let fleets = document.getElementsByClassName("table519")[0]
    let f_rows = fleets.getElementsByTagName("tr")
    for (let i = 2; i < f_rows.length; i++) {
        let tds = f_rows[i].getElementsByTagName("td")
        if (tds.length !== 4) {
            continue
        }
        let name = tds[0].innerText.trim()
        let idx = battleships.indexOf(name)
        if (idx > bestid) {
            bestname = name
            bestscore = exp_Values[name]
            bestid = idx
        }


    }

    return [bestname, bestscore]
}


function fleet_expo() {
    if (!window.location.href.includes("pr0game.com/game.php?page=fleetTable")) {
        return
    }
    let beststuff = get_best_ship_type()
    let fleets = document.getElementsByClassName("table519")[0]
    let f_rows = fleets.getElementsByTagName("tr")
    let gts_needed = Math.ceil((GM_getValue("exp_max", -1) - beststuff[1]) / 60)

    for (let i = 2; i < f_rows.length; i++) {
        let tds = f_rows[i].getElementsByTagName("td")
        if (tds.length !== 4) {
            continue
        }
        let name = tds[0].innerText.trim()
        if (name === "Spionagesonde" || name === "Spy Probe") {
            tds[3].firstChild.value = 1
            continue
        }
        if (name === beststuff[0]) {
            tds[3].firstChild.value = 1
            continue

        }


        if (name !== "Großer Transporter" && name !== "Heavy Cargo") {
            tds[3].firstChild.value = 0
            continue
        }
        let amt = Number(tds[1].innerText.replaceAll(".", ""))
        if (amt < gts_needed) {
            alert("Not at expo cap!")
        }
        tds[3].firstChild.value = Math.min(amt, gts_needed)

    }
    show_exp_value();
}


function custom_expo() {
    if (!window.location.href.includes("pr0game.com/game.php?page=fleetTable")) {
        return
    }
    noShips();
    const custom_fleet = GM_getValue("custom_fleets", {})[this.getAttribute("cfleet")]
    let warn = false;
    for (let stype in custom_fleet) {
        let obj = document.getElementById(`ship${stype}_input`)
        if (obj) {
            obj.value = custom_fleet[stype]

        } else {
            if (custom_fleet[stype] != "0") {
                warn = true;
            }
        }
    }
    show_exp_value();
    if (warn) {
        alert("not all ships could be selected!")
    }
}

function gt_expo() {
    if (!window.location.href.includes("pr0game.com/game.php?page=fleetTable")) {
        return
    }

    let fleets = document.getElementsByClassName("table519")[0]
    let f_rows = fleets.getElementsByTagName("tr")
    let gts_needed = Math.ceil(GM_getValue("exp_max", -1) / 60)

    for (let i = 2; i < f_rows.length; i++) {

        let tds = f_rows[i].getElementsByTagName("td")
        if (tds.length !== 4) {
            continue
        }
        let name = tds[0].innerText.trim()
        if (name === "Spionagesonde" || name === "Spy Probe") {
            tds[3].firstChild.value = 1
            continue
        }

        if (name !== "Großer Transporter" && name !== "Heavy Cargo") {
            tds[3].firstChild.value = 0
            continue
        }
        let amt = Number(tds[1].innerText.replaceAll(".", ""))
        tds[3].firstChild.value = Math.min(amt, gts_needed)
        if (amt < gts_needed) {
            alert("Not at expo cap!")
        }

    }
    show_exp_value();
}


function fleet_showcustom() {
    let cfleet = GM_getValue("custom_fleets", {})
    let x = ""
    for (let k in cfleet) {
        x += '   <a cfleet="' + k + '" class="cfleet" href="javascript:;">' + k + '</a>'

    }
    return x;
}

export function add_expo_buttons() {
    if (!window.location.href.includes("pr0game.com/game.php?page=fleetTable")) {
        return
    }
    if (document.getElementsByClassName("table519")[0].getElementsByTagName("tr")[0].innerText.includes(":") === false) {
        return;
    }

    let fleets = document.getElementsByClassName("table519")[0]

    let f_rows = fleets.getElementsByTagName("tr")

    let ex = fleet_showcustom()
    let row = document.createElement("tr");


    row.innerHTML += `<td colspan="4"><center>
<a id="gt_expo" href="javascript:;">GT_expo</a>
<a id="fleet_expo" href="javascript:;">Fleet_expo</a>
` + ex + `</center></td> `
    fleets.childNodes[1].insertBefore(row, f_rows[f_rows.length - 1]);
    document.getElementById("gt_expo").addEventListener("click", gt_expo);
    document.getElementById("fleet_expo").addEventListener("click", fleet_expo);
    for (let node of document.querySelectorAll(".cfleet")) {

        node.addEventListener("click", custom_expo);
    }
    let sdiv = `<center id="res_sender">Met:<input type="number" value="${document.getElementById("current_metal").innerText.replace(/\./g, "")}"> Krist:<input type="number" value="${document.getElementById("current_crystal").innerText.replace(/\./g, "")}"> Deut:<input type="number" value="${document.getElementById("current_deuterium").innerText.replace(/\./g, "")}"> <br><button id="gt_send" dt="ship203_input">gt (42)</button> <button id="kt_send" dt="ship202_input">kt (132)</button><br></center>`

    let x = document.createElement("div");
    x.innerHTML = sdiv
    document.getElementsByTagName("content")[0].insertBefore(x, document.getElementsByTagName("content")[0].childNodes[6])
    for (let ipt of x.getElementsByTagName("input")) {

        ipt.addEventListener('change', set_tranport_buttons);
    }
    for (let ipt of x.getElementsByTagName("button")) {

        ipt.addEventListener('click', set_transport_amt);
    }
    set_tranport_buttons();
}

function set_tranport_buttons() {
    let ttl = 0
    for (let ipt of document.getElementById("res_sender").getElementsByTagName("input")) {
        ttl += parseInt(ipt.value)
    }

    let gts = Math.floor(ttl / 25000)
    let kts = Math.floor(ttl / 5000)
    if (ttl / 25000 - gts > 0.8) {
        gts += 2
    } else {
        gts += 1
    }
    if (ttl / 5000 - kts > 0.6) {
        kts += 2
    } else {
        kts += 1
    }
    document.getElementById("gt_send").innerText = "gt select (" + gts + ")"
    document.getElementById("kt_send").innerText = "kt select (" + kts + ")"
    document.getElementById("gt_send").setAttribute("data", gts)
    document.getElementById("kt_send").setAttribute("data", kts)
    document.getElementsByClassName("table519")[0].querySelector('input[type="submit"]')?.addEventListener('click', save_trans_res);
}

function set_transport_amt() {
    let amt = this.getAttribute("data")
    let stype = this.getAttribute("dt")
    noShips()
    document.getElementById(stype).value = amt;

}

function save_trans_res() {
    let inpts = document.getElementById("res_sender").getElementsByTagName("input")
    GM_setValue("fleet_trans_res", [inpts[0].value, inpts[1].value, inpts[2].value]);
}



export function add_exp_eventlisteners() {
    if (!window.location.href.includes("pr0game.com/game.php?page=fleetTable")) {
        return
    }
    let fleets = document.getElementsByClassName("table519")[0]
    let f_rows = fleets.querySelectorAll("input")
    for (let i of f_rows) {
        i.addEventListener("change", () => setTimeout(show_exp_value, 100));
    }
    f_rows = fleets.querySelectorAll("a")
    for (let i of f_rows) {

        i.addEventListener("click", () => setTimeout(show_exp_value, 100));
    }
}


export function set_max_exp_score() {
    let stats = document.getElementById("stats")
    if (!stats) {
        return
    }
    if (stats.getElementsByTagName("select")[1].value != "1") {
        return
    }
    if (document.getElementsByClassName("tooltip")[0].innerText != "1") {
        return
    }

    let bestscore = parseInt(document.getElementsByClassName("table519")[1].getElementsByTagName("td")[4].innerText.replace(/\./g, ""))
    let exposcore = 2400
    if (bestscore > 100000) {
        exposcore = 6000
    }
    if (bestscore > 1000000) {
        exposcore = 9000
    }
    if (bestscore > 5000000) {
        exposcore = 12000
    }
    if (bestscore > 25000000) {
        exposcore = 15000
    }
    if (bestscore > 50000000) {
        exposcore = 18000
    }

    if (bestscore > 75000000) {
        exposcore = 21000
    }
    if (bestscore > 100000000) {
        exposcore = 25000
    }

    GM_setValue("exp_max", exposcore)
}

const exp_Values = {
    "Kleiner Transporter": 20,
    "Großer Transporter": 60,
    "Leichter Jäger": 20,
    "Schwerer Jäger": 50,
    "Kreuzer": 135,
    "Schlachtschiff": 300,
    "Kolonieschiff": 150,
    "Recycler": 80,
    "Spionagesonde": 5,
    "Bomber": 375,
    "Zerstörer": 550,
    "Todesstern": 45000,
    "Schlachtkreuzer": 350,
    "Solarsatellit": 0,

    "Light Cargo": 20,
    "Heavy Cargo": 60,
    "Light Fighter": 20,
    "Heavy Fighter": 50,
    "Cruiser": 135,
    "Battleship": 300,
    "Colony Ship": 150,
    "Spy Probe": 5,
    "Planet Bomber": 375,
    "Star Fighter": 550,
    "Battle Fortress": 45000,
    "Battle Cruiser": 350,
    "Solar Satellite": 0

}

const battleships = ["Leichter Jäger", "Schwerer Jäger", "Kreuzer", "Schlachtschiff", "Schlachtkreuzer", "Bomber", "Zerstörer", "Light Fighter", "Heavy Fighter", "Cruiser", "Battleship", "Battle Cruiser", "Planet Bomber", "Star Fighter"]
