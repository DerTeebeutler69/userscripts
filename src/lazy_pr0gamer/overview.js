import {pad} from "./general";

export function showexact_time() {
    if (!window.location.href.includes("page=overview")) {
        return
    }

    const mh = document.getElementById("hidden-div2").getElementsByTagName("li")
    for (let flight of mh) {
        let nd = new Date(parseInt(flight.firstChild.getAttribute("data-fleet-end-time")) * 1000);
        flight.innerHTML = '<span style="color:yellow">' + nd.getHours() + ":" + pad(nd.getMinutes(), 2) + ":" + pad(nd.getSeconds(), 2) + ' </span>| ' + flight.innerHTML
    }


}
