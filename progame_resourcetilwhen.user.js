// ==UserScript==
// @name         progame_resourcetilwhen
// @namespace    http://tampermonkey.net/
// @version      1.24.3
// @description  Userscript to make less clicks in pr0game and be more lazy
// @author       atain
// @include      /https:\/\/(www.|)pr0game\.com\/game\.php.*/
// @updateURL    https://codeberg.org/pr0game/userscripts/raw/branch/master/progame_resourcetilwhen.user.js
// @downloadURL  https://codeberg.org/pr0game/userscripts/raw/branch/master/progame_resourcetilwhen.user.js
// ==/UserScript==
/* globals dataFlyTime, calculateTransportCapacity, shortly_number, noShips, calculateRatios, serverTime */
// ACHTUNG: schlechter code!

(function () {
    'use strict';

    function GM_getValue(key, defaultv) {
        let x = localStorage.getItem(key);
        if (x === null) {
            return defaultv;
        }
        return JSON.parse(x)
    }

    function GM_setValue(key, value) {
        localStorage.setItem(key, JSON.stringify(value))
    }


    //expo sach
    const sizes = {1: "small", 2: "medium", 3: "big"}

    function get_current_planet_id() {
        let planets = document.getElementById("planetSelector")
        let pname = planets.querySelector("option[value='" + planets.value + "']").innerText.split("[")[1]
        if (planets.querySelector("option[value='" + planets.value + "']").innerText.includes("(")) {
            pname = pname + "M"
        }
        return pname

    }

    function debris_text() {
        if (!location.href.includes("page=galaxy")) {
            return;
        }
        const tfmin = GM_getValue("galaxy_tf_min", 0)
        for (let db of document.querySelectorAll("img[src *='debris']")) {
            let text = db.parentNode.getAttribute("data-tooltip-content")
            let [met, krist] = text.match(/(\d+\.)*\d+<\//g)
            met = parseInt(met.replace("</", "").replaceAll(".", ""))
            krist = parseInt(krist.replace("</", "").replaceAll(".", ""))
            let dnode = document.createElement("div")
            dnode.innerHTML = `M: ${shortly_number(met)}<br>K: ${shortly_number(krist)}`
            if (met + krist < tfmin) {
                continue
            }
            db.parentNode.appendChild(dnode)
        }
        let hack = document.getElementsByClassName("hack")[0]

        hack.innerHTML = `TF min:<input type="number" id="tf_min" value="${tfmin}">`
        document.getElementById("tf_min").addEventListener("change", set_debris_limit);

    }

    function set_debris_limit() {
        let nbr = parseInt(document.getElementById("tf_min").value)
        GM_setValue("galaxy_tf_min", nbr)
    }

    debris_text();




    function marketstuff() {
        if (!location.href.includes("page=marketPlace")) {
            return;
        }
        calculateRatios()
        document.getElementsByName("ratio-metal")[0].setAttribute("step", "0.1")
        document.getElementsByName("ratio-cristal")[0].setAttribute("step", "0.1")
        document.getElementsByName("ratio-deuterium")[0].setAttribute("step", "0.1")


    }



    function commander_building_reader() {
        if (!window.location.href.includes("page=buildings")) {
            return
        }
        let defbuildings = document.getElementsByClassName("buildn")
        const amtregex = /\d+/g
        const def_obj = {}
        for (let def of defbuildings) {
            let text = def.innerText.replaceAll(".", "");
            let id = def.firstElementChild.getAttribute('onclick').match(amtregex)
            let match = text.match(amtregex)
            if (match && match.length === 1 && id.length === 1) {
                def_obj[id[0]] = parseInt(match[0])
            }

        }
        const commander_def = GM_getValue("commander_building", {})
        commander_def[get_current_planet_id()] = def_obj
        GM_setValue("commander_building", commander_def)
    }

    function get_imperiom_stuff() {

        if (!window.location.href.includes("page=imperium")) {
            return
        }
        const now = Date.now()
        const tblrows = document.getElementsByTagName("content")[0].getElementsByTagName("tr")

        const pids = ["abc", "def"]


        let planets = document.getElementById("planetSelector")
        for (let p of planets.children) {
            let pname = p.innerText.split("[")[1]
            if (p.innerText.includes("(")) {
                pname = pname + "M"
            }
            pids.push(pname)
        }

        for (let i in tblrows) {
            let spt = String(tblrows[i].innerText).split("\t")
            if (spt.length === pids.length) {
                if (spt[1] == "0") {
                    tblrows[i].style.display = "None"
                }
            }

        }
        // res and production
        const res = GM_getValue("res", {})
        const prod = GM_getValue("prod", {})
        const met = tblrows[6].innerText.split("\t")
        const krist = tblrows[7].innerText.split("\t")
        const deut = tblrows[8].innerText.split("\t")
        const energy = tblrows[9].innerText.split("\t")
        const met_s = tblrows[20].innerText.split("\t")
        const krist_s = tblrows[21].innerText.split("\t")
        const deut_s = tblrows[22].innerText.split("\t")
        for (let i = 2; i < met.length; i++) {
            met[i] = met[i].replaceAll("/h", "")
            met[i] = met[i].replaceAll(".", "")
            let mt = met[i].split(" ")
            krist[i] = krist[i].replaceAll("/h", "")
            krist[i] = krist[i].replaceAll(".", "")
            let kt = krist[i].split(" ")
            deut[i] = deut[i].replaceAll("/h", "")
            deut[i] = deut[i].replaceAll(".", "")
            let dt = deut[i].split(" ")
            let engry = energy[i].replace(/\./g, "")
            prod[pids[i]] = {met: parseInt(mt[1]), krist: parseInt(kt[1]), deut: parseInt(dt[1])}

            res[pids[i]] = {
                time: now,
                energy: parseInt(engry),
                energy_max: 9999999,
                deut: parseInt(dt[0]),
                deut_max: get_storage_capacity(deut_s[i]),
                krist: parseInt(kt[0]),
                krist_max: get_storage_capacity(krist_s[i]),
                met: parseInt(mt[0]),
                met_max: get_storage_capacity(met_s[i])
            }
        }

        GM_setValue("res", res)
        GM_setValue("prod", prod)

        //gebäude
        const commander_building = GM_getValue("commander_building", {})

        let line_to_building = {
            11: 1,
            12: 2,
            13: 3,
            14: 4,
            16: 12,
            17: 14,
            19: 21,
            20: 22,
            21: 23,
            22: 24,
            23: 31,
            25: 34,
            29: 44
        }
        let line_vals = {}
        for (let line in line_to_building) {
            line_vals[line] = tblrows[line].innerText.split("\t")

        }
        for (let i = 2; i < met.length; i++) {
            let obj = {}
            for (let l in line_to_building) {
                obj[line_to_building[l]] = parseInt(line_vals[l][i])
            }
            commander_building[pids[i]] = obj
        }

        GM_setValue("commander_building", commander_building)

        //flotte

        const commander_fleet = GM_getValue("commander_fleet", {})
        const line_to_fleet = {
            61: 212,
            51: 202,
            52: 203,
            59: 210,
            58: 209,
            57: 208,
            53: 204,
            54: 205,
            55: 206,
            56: 207,
            64: 215,
            60: 211,
            62: 213,
            63: 214
        }
        line_vals = {}
        for (let line in line_to_fleet) {
            line_vals[line] = tblrows[line].innerText.split("\t")

        }

        for (let i = 2; i < met.length; i++) {
            let obj = {}
            for (let l in line_to_fleet) {
                if (line_vals[l].length !== met.length) {
                    continue
                }
                obj[line_to_fleet[l]] = parseInt(line_vals[l][i].replace(/\./g, ""))
            }
            commander_fleet[pids[i]] = obj
        }
        GM_setValue("commander_fleet", commander_fleet)

        //def
        const commander_def = GM_getValue("commander_def", {})
        let line_to_def = {
            66: "s401",
            67: "s402",
            68: "s403",
            69: "s404",
            70: "s405",
            72: "s407",
            73: "s408",
            71: "s406",
            75: "s502",
            76: "s503"
        }
        line_vals = {}
        for (let line in line_to_def) {
            line_vals[line] = tblrows[line].innerText.split("\t")

        }
        for (let i = 2; i < met.length; i++) {
            let obj = {}
            for (let l in line_to_def) {
                if (line_vals[l].length !== met.length) {
                    continue
                }
                obj[line_to_def[l]] = parseInt(line_vals[l][i].replace(/\./g, ""))
            }
            commander_def[pids[i]] = obj
        }

        GM_setValue("commander_def", commander_def)

    }

    function get_storage_capacity(lvl) {
        return Math.floor(2.5 * Math.exp(20 * lvl / 33)) * 5000
    }

    function commander_building_table() {
        const commander_def = GM_getValue("commander_building", {})
        let html = "   <table>        <tr>          <th>Buildings</th>";
        let shiptypes = {
            "1": "Met",
            "2": "Krist",
            "3": "Deut",
            "4": "Skw",
            "12": "Fkw",
            "14": "Robo",
            "21": "Werft",
            "22": "Met S",
            "23": "Krist S",
            "24": "Deut S",
            "31": "Lab",
            "34": "Idiot - really this is stupid",
            "44": "Silo"
        }
        let showntypes = {};
        let sary = ["1", "2", "3", "4", "12", "22", "23", "24", "14", "21", "31", "44", "34"]
        let showary = []
        for (let k of sary) {
            let amt = commander_fleet_check_fleet_show(k, commander_def)
            if (amt > 0) {
                showntypes[k] = amt
                showary.push(k)
            }
        }
        let moons = 0
        for (let ko of get_sortet_planet_list(Object.keys(commander_def))) {
            if (ko.includes("M")) {
                moons += 1
            }

        }
        let amtplanets = Object.keys(commander_def).length - moons
        for (let sid of showary) {
            html += `<th>${shiptypes[sid]}</th>`
        }

        html += "</tr><th>Mean</th>"
        for (let sid of showary) {
            html += `<th>${Math.round(10 * showntypes[sid] / amtplanets) / 10}</th>`

        }

        html += "</tr>"
        for (let ko of get_sortet_planet_list(Object.keys(commander_def))) {
            html += `<tr><th>[${ko}</th>`
            for (let sid of showary) {
                if (!(sid in commander_def[ko])) {
                    commander_def[ko][sid] = 0
                }
                html += `<td>${commander_def[ko][sid]}</td>`

            }
            html += "</tr>"

        }
        return html + "</table>"
    }

    function commander_def_reader() {
        if (!window.location.href.includes("mode=defense")) {
            return
        }
        let defbuildings = document.getElementsByTagName("form")
        const amtregex = /\d+/g
        const def_obj = {}
        for (let def of defbuildings) {
            let text = def.firstElementChild.innerText.replaceAll(".", "");
            let match = text.match(amtregex)
            if (match && match.length === 1 && def.id) {
                def_obj[def.id] = parseInt(match[0])
            }

        }
        const commander_def = GM_getValue("commander_def", {})
        commander_def[get_current_planet_id()] = def_obj
        GM_setValue("commander_def", commander_def)
    }

    function commander_def_table() {
        const commander_def = GM_getValue("commander_def", {})
        let html = "   <table>        <tr>          <th>Def</th>";
        let shiptypes = {
            "s401": "Rak",
            "s402": "LL",
            "s403": "SL",
            "s404": "Gauß",
            "s405": "Ion",
            "s407": "Ks",
            "s408": "Gs",
            "s406": "Pw",
            "s502": "Ar",
            "s503": "Irak"
        }
        let showntypes = {};
        let sary = ["s401", "s402", "s403", "s405", "s404", "s406", "s502", "s407", "s408"]
        let showary = []
        for (let k of sary) {
            let amt = commander_fleet_check_fleet_show(k, commander_def)
            if (amt > 0) {
                showntypes[k] = amt
                showary.push(k)
            }
        }
        for (let sid of showary) {
            html += `<th>${shiptypes[sid]}</th>`
        }

        html += "</tr><th>Total</th>"
        for (let sid of showary) {
            html += `<th>${showntypes[sid]}</th>`

        }

        html += "</tr>"
        for (let ko of get_sortet_planet_list(Object.keys(commander_def))) {
            html += `<tr><th>[${ko}</th>`
            for (let sid of showary) {
                if (!(sid in commander_def[ko])) {
                    commander_def[ko][sid] = 0
                }
                html += `<td>${commander_def[ko][sid]}</td>`

            }
            html += "</tr>"

        }
        return html + "</table>"
    }

    function set_custom_fleet() {


        return `
<table  id="customfleet">
<tr>
<th colspan=3><select id="cfleet_select" style="width:100%">

</select></th>
</tr>
<tr><th colspan=3><input id="cfleet_name" placeholder="name" style="width:90%"></th>
</tr>
<tr>
<td colspan=1 >Expo points:</td>
<td colspan=2 id="ship_expo_points"></td>
</tr>


<tr>
          <th>Shiptype</th>
          <th colspan=2>Amount </th>
        </tr>
        <tr>
          <td>KT</td>
          <td colspan=2><input id="ship_202" type="number" value="0"></td>
        </tr>
         <tr>
          <td>GT</td>
          <td colspan=2><input id="ship_203" type="number" value="0"></td>
        </tr>
        <tr>
          <td>LJ</td>
          <td colspan=2><input id="ship_204" type="number" value="0"></td>
        </tr>
        <tr>
          <td>SJ</td>
          <td colspan=2><input id="ship_205" type="number" value="0"></td>
        </tr>
         <tr>
          <td>Xer</td>
          <td colspan=2><input id="ship_206" type="number" value="0"></td>
        </tr>
                <tr>
          <td>SS</td>
          <td colspan=2><input id="ship_207" type="number" value="0"></td>
        </tr>
                <tr>
          <td>Sxer</td>
          <td colspan=2><input id="ship_215" type="number" value="0"></td>
        </tr>
         <tr>
          <td>Zerr</td>
          <td colspan=2><input id="ship_213" type="number" value="0"></td>
        </tr>
         <tr>
          <td>Bomber</td>
          <td colspan=2><input id="ship_211" type="number" value="0"></td>
        </tr>
        <tr>
                    <td>RIP</td>
          <td colspan=2><input id="ship_214" type="number" value="2"></td>
        </tr>

         <tr>
              <td>Spio</td>
          <td colspan=2><input id="ship_210" type="number" value="0"></td>
        </tr>
         <tr>
              <td>Recycler</td>
          <td colspan=2><input id="ship_209" type="number" value="0"></td>
        </tr>
        <tr>
              <td>Kolo</td>
          <td colspan=2><input id="ship_208" type="number" value="0"></td>
        </tr>

   <tr>
              <td colspan=3><center><button id="cf_save" style="width:25%">
              Save
              </button><button  id="cf_del" style="width:25%">
              Remove
              </button></center></td>
        </tr>



</table>
        `


    }



    function commander_fleet_check_fleet_show(id, commander_fleet) {
        let ttx = 0
        for (let ko in commander_fleet) {
            if (!(id in commander_fleet[ko])) {
                commander_fleet[ko][id] = 0
            }
            ttx += parseInt(String(commander_fleet[ko][id]).replaceAll(".", ""))

        }
        return ttx

    }

    function get_sortet_planet_list(planets) {
        return planets.sort(comperator_planetids)

    }

    function comperator_planetids(a, b) {
        let spta = a.split(":")
        let sptb = b.split(":")

        let ma = ("" + spta[2]).includes("M")
        if (ma) {
            spta[2] = spta[2].slice(0, -1)
        }
        let mb = ("" + sptb[2]).includes("M")
        if (mb) {
            sptb[2] = sptb[2].slice(0, -1)
        }
        spta[2] = spta[2].slice(0, -1)
        sptb[2] = sptb[2].slice(0, -1)


        if (parseInt(spta[0]) !== parseInt(sptb[0])) {
            if (parseInt(spta[0]) > parseInt(sptb[0])) {
                return 1
            }
            return -1
        }
        if (parseInt(spta[1]) !== parseInt(sptb[1])) {
            if (parseInt(spta[1]) > parseInt(sptb[1])) {
                return 1
            }
            return -1
        }

        if (parseInt(spta[2]) > parseInt(sptb[2])) {
            return 1
        }
        if (parseInt(spta[2]) < parseInt(sptb[2])) {
            return -1
        }
        if (ma) {
            return -1
        }
        return 1
    }

    function commander_fleet_table_invert() {
        const commander_fleet = GM_getValue("commander_fleet", {})
        let html = "   <table>        <tr>          <th>Shiptype</th>";
        let shiptypes = {
            212: "Sats",
            202: "KT",
            203: "GT",
            210: "Spio",
            209: "Rec",
            208: "Kolo",
            204: "LJ",
            205: "SJ",
            206: "Xer",
            207: "SS",
            215: "Sxer",
            211: "Bomber",
            213: "Zerr",
            214: "RIP"
        }
        let showntypes = {};
        let sary = [212, 202, 203, 210, 209, 208, 204, 205, 206, 207, 215, 211, 213, 214]
        let showary = []
        for (let k of sary) {
            let amt = commander_fleet_check_fleet_show(k, commander_fleet)
            if (amt > 0) {
                showntypes[k] = amt
                showary.push(k)
            }
        }
        for (let sid of showary) {
            html += `<th>${shiptypes[sid]}</th>`
        }
        html += "</tr><th>Total</th>"
        for (let sid of showary) {
            html += `<th>${showntypes[sid]}</th>`

        }
        html += "</tr>"

        for (let ko of get_sortet_planet_list(Object.keys(commander_fleet))) {
            html += `<tr><th>[${ko}</th>`
            for (let sid of showary) {
                if (!(sid in commander_fleet[ko])) {
                    commander_fleet[ko][sid] = 0
                }
                html += `<td>${commander_fleet[ko][sid]}</td>`

            }
            html += "</tr>"

        }


        return html + "</table>"

    }

    function commander_res_table() {
        const res = GM_getValue("res", {})
        const livve_vals = get_current_values()
        let html = "   <table>        <tr>          <th>Res</th>";
        const types = ["met", "krist", "deut", "energy"]
        for (let sid of types) {
            html += `<th>${sid}</th>`
        }
        html += "</tr>"
        for (let ko of get_sortet_planet_list(Object.keys(res))) {
            html += `<tr><th>[${ko}</th>`
            for (let sid of types) {
                if (!(sid in res[ko])) {
                    res[ko][sid] = -1
                }

                let maxc = "-1"
                if (typeof livve_vals[ko] == "undefined" || typeof livve_vals[ko][sid] == "undefined" || typeof res[ko] == "undefined" || typeof res[ko][sid + "_max"] == "undefined") {
                    html += `<td style='color:gray;font-weight: bolder;'>???</td>`
                    break;
                }
                maxc = 120 - Math.round(120 * Math.min(1, livve_vals[ko][sid] / res[ko][sid + "_max"]))

                if (sid == "energy") {
                    if (livve_vals[ko][sid] < 0) {
                        maxc = 0
                    }
                    html += `<td style='color:hsl(${maxc},100%,50%);font-weight: bolder;'>${livve_vals[ko][sid]}</td>`
                } else {
                    html += `<td style='color:hsl(${maxc},100%,50%);font-weight: bolder;'>${shortly_number(livve_vals[ko][sid])}</td>`
                }

            }
            html += "</tr>"

        }


        return html + "</table>"

    }


    const pirates = {
        1: ["Ein paar anscheinend sehr verzweifelte Weltraumpiraten haben versucht, unsere Expeditionsflotte zu kapern.",
            "Einige primitive Barbaren greifen uns mit Raumschiffen an, die nicht einmal ansatzweise die Bezeichnung Raumschiff verdient haben. Sollte der Beschuss ernst zu nehmende Ausmaße annehmen, sehen wir uns gezwungen das Feuer zu erwidern.",
            "Wir haben ein paar Funksprüche sehr betrunkener Piraten aufgefangen. Anscheinend sollen wir überfallen werden.",
            "Wir mussten uns gegen einige Piraten wehren, die zum Glück nicht allzu zahlreich waren.",
            "Unsere Expeditionsflotte meldet, dass ein gewisser Moa Tikarr und seine wilde Meute die bedingungslose Kapitulation unserer Flotte verlangen. Sollten sie Ernst machen, werden sie feststellen müssen, dass sich unsere Schiffe durchaus zu wehren wissen.",
            'A space pirate in despair tried to commandeer our expeditions fleet.',
            'Some primitive barbarians attacked us with spaceships, at least their ships were in horrible conditions. If the firepower was a serious threat we would have beene forced to reply with our firepower.',
            'We have intercepted a couple of radio messages of some very drunk pirates. We apparently will be attacked.',
            'We had to fight against some pirates who fortunately were outnumbered against us.',
            'Our fleet reports that a certain Moa Tikarr and his crew requested the unconditional surrender of our fleet. If they are serious, they will have to find out that we can defend ourself very well.'
        ],
        2: ["Deine Expeditionsflotte hatte ein unschönes Zusammentreffen mit einigen Weltraumpiraten.",
            "Wir sind in den Hinterhalt einiger Sternen-Freibeuter geraten! Ein Kampf war leider unvermeidlich.",
            "Der Hilferuf, dem die Expedition folgte, stellte sich als böse Falle einiger arglistiger Sternen-Freibeuter heraus. Ein Gefecht war unvermeidlich.",
            'Your expedition fleet had an unforseen encounter with some spacepirates.',
            'We flew into an ambush of some starpirates! Unfortunately a fight was unavoidable.',
            'The call for help of another expeditionfleet turned out to be a trap of some cunning pirates. A battle was unavoidable.'
        ],
        3: ["Die aufgefangenen Signale stammten nicht von Fremdwesen, sondern von einer geheimen Piratenbasis! Die Piraten waren von unserer Anwesenheit in ihrem Sektor nicht besonders begeistert.",
            "Die Expeditionsflotte meldet schwere Kämpfe mit nicht-identifizierten Piratenschiffen!",
            'Wir haben gerade eine dringende Nachricht vom Expeditionskommandanten erhalten: "Sie kommen auf uns zu! Sie sind aus dem Hyperraum gesprungen, zum Glück sind es nur Piraten, wir haben also eine Chance, wir werden kämpfen!"',
            'The intercepted signals did not came from some unknown aliens but of a secret pirate base! The pirates were not very happy by our presence in their sector.',
            'The expedition fleet reported heavy fights with unidentified pirate ships!',
            'We have just received an urgent message from the expedition commander: "They\'re coming at us! They jumped out of hyperspace, fortunately they\'re just pirates, so we have a chance, we\'ll fight!"'
        ]
    }
    const aliens = {
        1: ['Deine Expeditionsflotte hatte einen nicht besonders freundlichen Erstkontakt mit einer unbekannten Spezies.',
            'Einige fremdartig anmutende Schiffe haben ohne Vorwarnung die Expeditionsflotte angegriffen!',
            'Unsere Expedition wurde von einer kleinen Gruppe unbekannter Schiffe angegriffen!',
            'Die Expeditionsflotte meldet Kontakt mit unbekannten Schiffen. Die Funksprüche sind nicht entschlüsselbar, jedoch scheinen die fremden Schiffe ihre Waffen zu aktivieren.',
            'Your expedition fleet had an uninteresting first contact with new aliens.',
            'Some strange ships have attacked the expedition fleet without early warning!',
            'Our expedition was attacked by a small group of unknown ships!',
            'The expedition fleet reports contact with unknown ships. The radio messages are not able to be decoded, however, the strange ships seem to activate their weapons.'
        ],
        2: ['Eine unbekannte Spezies greift unsere Expedition an!',
            'Deine Expeditionsflotte hat anscheinend das Hoheitsgebiet einer bisher unbekannten, aber äußerst aggressiven und kriegerischen Alienrasse verletzt.',
            'Die Verbindung zu unserer Expeditionsflotte wurde kurzfristig gestört. Soweit wir die letzte Botschaft richtig entschlüsselt haben, steht die Flotte unter schwerem Feuer – die Aggressoren konnten nicht identifiziert werden.',
            'An unknown specie attacks our expedition!',
            'Your expedition fleet has apparently violated the territory of a previously unknown but highly aggressive alien race.',
            'The connection with our expedition fleet was disconnected abruptly. The last what we heard of the fleet was that it was taking heavy fire - the aggressors could not be identified.'],
        3: ['Deine Expedition ist in eine Alien-Invasions-Flotte geraten und meldet schwere Gefechte!',
            'Ein großer Verband kristalliner Schiffe unbekannter Herkunft hält direkten Kollisionskurs mit unserer Expeditionsflotte. Wir müssen nun wohl vom Schlimmsten ausgehen.',
            'Wir hatten ein wenig Schwierigkeiten, den Dialekt der fremden Rasse richtig auszusprechen. Unser Diplomat nannte versehentlich "Feuer!" statt "Frieden!".',
            'Your expedition has come across an alien invasion fleet and reports massive battles!',
            'A large group of crystalline ships of unknown origin is on a direct collision course with our expedition fleet. We must prepare ourself for the worst.',
            'We had a bit of difficulty pronouncing the dialect of the alien race correctly. Our diplomat accidentally called']
    }

    const res_found = {
        1: ['Deine Expedition hat einen kleinen Asteroidenschwarm entdeckt, aus dem einige Ressourcen gewonnen werden können.',
            'Auf einem abgelegenen Planetoiden wurden einige leicht zugängliche Ressourcenfelder gefunden und erfolgreich Rohstoffe gewonnen.',
            'Deine Expedition stieß auf sehr alte Raumschiffwracks einer längst vergangenen Schlacht. Einzelne Komponenten konnte man bergen und recyceln.',
            'Die Expedition stieß auf einen radioaktiv verstrahlten Planetoiden mit hochgiftiger Atmosphäre. Jedoch ergaben Scans, dass dieser Planetoid sehr rohstoffhaltig ist. Mittels automatischer Drohnen wurde versucht, ein Maximum an Rohstoffen zu gewinnen.',
            'Your expedition has discovered a small asteroids cluster from which some resources could be won.',
            'Some easily accessible resource fields were found and raw materials were won successfully on the remote planetoid.',
            'The expedition discovered a planet contaminated by radiation with a highly-poisonous atmosphere. Scans, however, shown that this planet contains alot of raw material. It was mined by automatic drones to win a maximum of raw materials.'
        ],
        2: ['Deine Expedition fand einen uralten, voll beladenen, aber menschenleeren Frachterkonvoi. Einige Ressourcen konnten geborgen werden.',
            'Auf einem kleinen Mond mit eigener Atmosphäre fand deine Expedition große Rohstoffvorkommen. Die Bodencrews sind dabei diese natürlichen Schätze zu heben.',
            'Wir haben einen kleinen Konvoi ziviler Schiffe getroffen, die dringend Nahrung und Medikamente benötigten. Im Austausch dafür erhielten wir eine ganze Menge nützlicher Ressourcen.',
            'Your expedition found an ancient freighter convoy which is fully loaded but deserted. Some resources could be yielded.',
            'Your expedition found great raw material deposits on a little moon with an atmosphere of it`s own. The ships crew is going to yield this treasure.',
            'We have met a small convoy of civilian ships which urgently needed food and medicine. In the exchange for it we got a whole set of useful resources.'],
        3: ['Deine Expeditionsflotte meldet den Fund eines riesigen Alien-Schiffswracks. Mit der Technologie konnten sie zwar nichts anfangen, aber das Schiff ließ sich in seine Einzelteile zerlegen und dadurch konnte man wertvolle Rohstoffe gewinnen.',
            'Ein Mineraliengürtel um einen unbekannten Planeten enthielt Unmengen an Rohstoffen. Die Expeditionsflotte meldet volle Lager!',
            'Your expedition fleet reported the find of a gigantic alien shipwreck. But the ships were of no purpose because its technology, however the ship could be chopped into pieces for its components so the crew could win valuable raw materials.',
            'The asteroid belt around an unknown planet contained vast amounts of raw materials. The expedition fleet filled its storage rooms!']
    }

    const ship_found = {
        1: ['Wir sind auf die Überreste einer Vorgängerexpedition gestoßen! Unsere Techniker schauen, ob sie einige der Wracks wieder Flugfähig bekommen',
            'Wir haben eine verlassene Piratenbasis gefunden. Im Hangar liegen noch einige alte Schiffe. Unsere Techniker schauen nach, ob einige davon noch zu gebrauchen sind.',
            'Unsere Expedition fand einen Planeten, der wohl durch anhaltende Kriege fast komplett zerstört wurde. In der Umlaufbahn trieben diverse Schiffswracks. Die Techniker versuchen, einige davon zu reparieren. Vielleicht erhalten wir so auch Information darüber, was hier geschehen ist.',
            'Deine Expedition ist auf eine alte Sternenfestung gestoßen, die wohl seit Ewigkeiten verlassen ist. Im Hangar der Festung wurden ein paar Schiffe gefunden. Die Techniker schauen, ob sie einige davon wieder flott bekommen.',
            'We have discovered the remains of a predecessor expedition! Our technicians look, whether they get some of the wrecks working again.',
            'We have found a deserted pirate base. Some old ships are still located in the hangar. Our technicians look, whether still some of the ships are useable.',
            'Our expedition found a planet which was almost completly destroyed by wars. Various shipwrecks drifted in the orbit. The technicians try to repair some of these. Perhaps we also find information on what has happened here.',
            'The expedition stumbled upon an old starbase which seems to be abandoned for an eternity. A couple of ships were found in the hangar of the fortress. The technicians managed to get some of them flying again.'],
        2: ['Wir haben die Reste einer Armada gefunden. Die Techniker der Expeditionsflotte haben sich sofort auf die halbwegs intakten Schiffe begeben und versuchen, diese wieder instandzusetzen.',
            'Unsere Expedition stieß auf eine alte, automatische Schiffswerft. Einige Schiffe sind noch in der Produktionsphase, und unsere Techniker versuchen, die Energieversorgung der Werft wiederherzustellen.',
            'We have found the remains of an armada. The technicians of the expedition have immediately gone onto the partly intact ships and were able to repair some of them.',
            'Our expedition discovered an old, automatic shipyard. Some ships still were in the production phase and our technicians tried to finish the construction, the energy supply of the shipyard first needed to be restored.'],
        3: ['Wir haben einen riesigen Raumschiffsfriedhof gefunden. Einigen Technikern der Expeditionsflotte ist es gelungen, das eine oder andere Schiff wieder in Betrieb zu nehmen.',
            'Wir haben einen Planeten mit Resten einer Zivilisation entdeckt. Aus dem Orbit ist noch ein riesiger Raumbahnhof zu erkennen, der als einziges Gebäude noch intakt ist. Einige unserer Techniker und Piloten haben sich auf die Oberfläche begeben um nachzuschauen, ob ein paar der dort abgestellten Schiffe noch zu gebrauchen sind.',
            'We have found a gigantic ship cemetery. Some technicians of the expedition have managed to get the ship into operation again.',
            'We have discovered a planet with remains of a civilization. Another gigantic ship station which is still intact has been spotted from outerspace. Some of our technicians and pilots have gone onto the surface to look, whether a couple of the ships can be retrieved.']
    }

    const return_speed = {
        1: ['Eine unvorhergesehene Rückkopplung in den Energiespulen der Antriebsaggregate beschleunigte den Rücksprung der Expedition, so dass sie nun früher als erwartet zurückkehrt. Ersten Meldungen zufolge hat sie jedoch nichts Spannendes zu berichten.',
            'Der etwas wagemutige neue Kommandant nutzte ein instabiles Wurmloch, um den Rückflug zu verkürzen – mit Erfolg! Jedoch hat die Expedition selbst keine neuen Erkenntnisse gebracht.',
            'Deine Expedition meldet keinen Besonderheiten in dem erforschten Sektor. Jedoch geriet die Flotte beim Rücksprung in einen Sonnenwind. Dadurch wurde der Sprung ziemlich beschleunigt. Deine Expedition kehrt nun etwas früher nach Hause.',
            'An unforeseen relay in the energy coils of the drive aggregates accelerated the return of the expedition. According to first reports it has however, nothing thrilling to report.',
            'A new daring commanding officer used an unstable wormhole as a shortcut to return home, with succes! The expedition, however, has not brought new knowledge itself.',
            'No unusual features in the investigated sector have been found on your expedition. The fleet, however, got into a solar wind at the return flight. Because of this your expedition returned a bit earlier as expected.'],
        2: [
            'Ein böser Patzer des Navigators führte zu einer Fehlkalkulation beim Sprung der Expedition. Nicht nur landete die Flotte an einem völlig falschen Ort, auch der Rückweg nahm nun erheblich mehr Zeit in Anspruch.',
            'Aus bisher unbekannten Gründen, ging der Sprung der Expeditionsflotte völlig daneben. Beinahe wäre man im Herzen einer Sonne herausgekommen. Zum Glück ist man in einem bekannten System gelandet, jedoch wird der Rücksprung länger dauern als ursprünglich gedacht.',
            'Das neue Navigationsmodul hat wohl doch noch mit einigen Bugs zu kämpfen. Nicht nur ging der Sprung der Expeditionsflotte in die völlig falsche Richtung, auch wurde das gesamte Deuterium verbraucht, wobei der Sprung der Flotte nur knapp hinter dem Mond des Startplaneten endete. Etwas enttäuscht kehrt die Expedition nun auf Impuls zurück. Dadurch wird die Rückkehr wohl ein wenig verzögert.',
            'Deine Expedition geriet in einen Sektor mit verstärkten Partikelstürmen. Dadurch überluden sich die Energiespeicher der Flotte und bei sämtlichen Schiffen fielen die Hauptsysteme aus. Deine Mechaniker konnten das Schlimmste verhindern, jedoch wird die Expedition nun mit einiger Verspätung zurückkehren.',
            'Das Führungsschiff deiner Expeditionsflotte kollidierte mit einem fremden Schiff, das ohne Vorwarnung direkt in die Flotte sprang. Das fremde Schiff explodierte und die Schäden am Führungsschiff waren beachtlich. Sobald die gröbsten Reparaturen abgeschlossen sind, werden sich deine Schiffe auf den Rückweg machen, da in diesem Zustand die Expedition nicht fortgeführt werden kann.',
            'Der Sternwind eines roten Riesen verfälschte den Sprung der Expedition dermaßen, dass es einige Zeit dauerte, den Rücksprung zu berechnen. Davon abgesehen gab es in dem Sektor, in dem die Expedition herauskam, nichts außer der Leere zwischen den Sternen.',
            'A miscalculation of the navigator at the expedition landed the fleet at a completely wrong place, the way back took substantially more time now.',
            'For some unknown reasons the jump of the expedition fleet missed it`s target completely. The fleet almost would have come out in the heart of a sun. Fortunately it landed in a known system, the return will, however, last longer than thought originally.',
            'The new navigation module still has a few bugs in it. The expedition fleet did not only go in the completely wrong direction, the complete deuterium was also consumed, because of this the return is delayed a little.',
            'Your expedition got into a sector with amplified particle storms. The energy storages of the fleet overloaded themselves because of this and for all ships the main systems were cancelled. Your mechanics could prevent the worst but the expedition will return with some delay now.',
            'The commandship of your expedition fleet collided with a strange ship which jumped directly in front of the fleet without early warning. The strange ship exploded and the damages to the leadership ship were considerable. Wanting to as soon as the roughest repairs are completed make your ships to themselves on the way back, since in this condition the expedition cannot be continued.',
            'The star wind of a red giant distorted the jump of the expedition so that the ships navigationsystem calculated the reentry incorrect. As a result the expedition return somewhere in the emptiness between the stars and the destination.',
        ]
    }

    const black_hole = {
        1: ['Von der Expedition ist nur noch folgender Funkspruch übrig geblieben: Zzzrrt Oh Gott! Krrrzzzzt das zrrrtrzt sieht krgzzzz ja aus wie Krzzzzzzzztzzzz...',
            'Das Letzte, was von dieser Expedition noch gesendet wurde, waren einige unglaublich gut gelungene Nahaufnahmen eines sich öffnenden, schwarzen Loches.',
            'Ein Kernbruch des Führungsschiffes führte zu einer Kettenreaktion, die in einer durchaus spektakulären Explosion die gesamte Expedition vernichtete.',
            'Die Expeditionsflotte ist nicht mehr aus dem Sprung in den Normalraum zurückgekehrt. Unsere Wissenschaftler rätseln noch immer, was geschehen sein könnte, jedoch scheint die Flotte endgültig verloren zu sein.',
            'The only thing left of the expedition is the following radio message: Zzzrrt oh God!...Krrrzzzzt the zrrrtrzt sees krgzzzz yes from how Krzzzzzzzztzzzz...',
            'The last thing we received of the expedition were some unbelievable good close-ups of an opening black hole.',
            'A nuclear breach on one of the commanding ships led to a chain reaction which destroyed the complete expedition in a spectacular explosion.',
            'The expedition fleet has not returned from the hyperspacejump. Our scientists are still puzzled on what could have happened, the fleet however seems to be lost definitely.']
    }

    function add_to_db(key, nr, ships) {
        const expodb = GM_getValue("expo_counts", {
            "res": [0, 0, 0],
            "pirates": [0, 0, 0],
            "aliens": [0, 0, 0],
            "speed": [0, 0],
            "hole": [0],
            "ships": [0, 0, 0],
            "nothing": [0]
        })
        const ship_res_db = GM_getValue("expo_ship_res", {"res": [0, 0, 0], "fleet": {}})
        expodb[key][nr - 1] += 1
        if (key === "res" && ships != null) {
            expodb[key][nr - 1] -= 1
            ship_res_db["res"][0] += ships[0]
            ship_res_db["res"][1] += ships[1]
            ship_res_db["res"][2] += ships[2]
        }
        if (key === "ships") {
            for (let stype in ships) {
                if (!(stype in ship_res_db["fleet"])) {
                    ship_res_db["fleet"][stype] = 0
                }
                ship_res_db["fleet"][stype] += ships[stype]
            }
        }
        GM_setValue("expo_counts", expodb)
        GM_setValue("expo_ship_res", ship_res_db)
    }

    function check_att(text, atype) {
        for (let ptype in atype) {
            for (let txt of atype[ptype]) {
                if (text.includes(txt)) {
                    return ptype
                }
            }
        }
        return 0
    }

    function get_ships(text) {
        let ships = {}
        for (let sp of text) {
            if (sp.match(/^.+: \d+/g)) {
                let shipname = sp.split(":")[0].trim()
                let amt = parseInt(sp.split(":")[1].trim().replace(/\./g, ""))
                ships[shipname] = amt
            }

        }
        return ships
    }

    function get_res_found(text) {
        let res = [0, 0, 0]
        for (let sp of text) {
            let rgmtc = sp.trim().match(/(\d+\.?)+.+\./g)
            if (rgmtc == null) {
                continue
            }
            if (rgmtc[0].includes("href")) {
                continue
            }
            if (rgmtc) {
                let idx = 0
                if (sp.includes("ium")) {
                    idx = 2
                }
                if (sp.includes("sta")) {
                    idx = 1
                }
                let amt = parseInt(rgmtc[0].replaceAll(".", "").match(/\d+/g))
                res[idx] = amt
            }
        }
        return res
    }


    function selectmesages() {
        const expo_ids = GM_getValue("expo_handled_ids", {})
        const mesages = document.getElementsByClassName("message_head")
        for (let msg of mesages) {
            if (msg.childNodes[5].innerText == "Flottenhauptquatier" || msg.childNodes[5].innerText == "Fleet Headquarters") {
                let expo = msg.childNodes[7].innerText.includes("xpedit")
                let text = document.getElementsByClassName("messages_body " + msg.id)[0].innerHTML.split("<br>");
                const idv = msg.id
                if (expo) { //checking the events
                    let itext = document.getElementsByClassName("messages_body " + msg.id)[0].innerText;

                    let resf = check_att(itext, res_found)
                    if (resf != 0) {
                        if (expo_ids[idv] !== 1) {
                            expo_ids[idv] = 1
                            let res = get_res_found(text)
                            add_to_db("res", 1, res)
                            GM_setValue("expo_handled_ids", expo_ids)
                            add_to_db("res", resf)
                        }

                        document.getElementsByClassName("messages_body " + msg.id)[0].children[0].innerHTML += "<br> <b style='color:#90EE90'>res: " + sizes[resf] + "</b>"
                        continue
                    }

                    let shipf = check_att(itext, ship_found)
                    if (shipf !== 0) {
                        let ships = get_ships(text)
                        if (expo_ids[idv] !== 1) {
                            expo_ids[idv] = 1
                            GM_setValue("expo_handled_ids", expo_ids)
                            add_to_db("ships", shipf, ships)
                        }
                        document.getElementsByClassName("messages_body " + msg.id)[0].children[0].innerHTML += "<br> <b style='color:#90EE90'>ships: " + sizes[shipf] + "</b>"
                        continue
                    }
                    let pirate = check_att(itext, pirates)
                    if (pirate != 0) {
                        if (expo_ids[idv] !== 1) {
                            expo_ids[idv] = 1
                            GM_setValue("expo_handled_ids", expo_ids)
                            add_to_db("pirates", pirate)
                        }

                        document.getElementsByClassName("messages_body " + msg.id)[0].children[0].innerHTML += "<br> <b style='color:#FFFF00'>pirates: " + sizes[pirate] + "</b>"
                        continue
                    }
                    let alien = check_att(itext, aliens)
                    if (alien != 0) {
                        if (expo_ids[idv] !== 1) {
                            expo_ids[idv] = 1
                            GM_setValue("expo_handled_ids", expo_ids)
                            add_to_db("aliens", alien)
                        }

                        document.getElementsByClassName("messages_body " + msg.id)[0].children[0].innerHTML += "<br> <b style='color:#FF0000'>aliens: " + sizes[alien] + "</b>"
                        continue
                    }

                    let speed = check_att(itext, return_speed)
                    if (speed != 0) {
                        if (expo_ids[idv] !== 1) {
                            expo_ids[idv] = 1
                            GM_setValue("expo_handled_ids", expo_ids)
                            add_to_db("speed", speed)
                        }

                        document.getElementsByClassName("messages_body " + msg.id)[0].children[0].innerHTML += "<br> <b style='color:#FFFF00'>speed: " + {
                            1: "fast",
                            2: "slow"
                        }[speed] + "</b>"
                        continue
                    }
                    let bh = check_att(itext, black_hole)
                    if (bh != 0) {
                        if (expo_ids[idv] !== 1) {
                            expo_ids[idv] = 1
                            GM_setValue("expo_handled_ids", expo_ids)
                            add_to_db("hole", 1)
                        }

                        document.getElementsByClassName("messages_body " + msg.id)[0].children[0].innerHTML += "<br> <b style='color:#FF4500'>big oof</b>"
                        continue
                    }
                    if (expo_ids[idv] !== 1) {
                        expo_ids[idv] = 1
                        GM_setValue("expo_handled_ids", expo_ids)
                        add_to_db("nothing", 1)
                    }

                    document.getElementsByClassName("messages_body " + msg.id)[0].children[0].innerHTML += "<br> <b style='color:#FFFFFF'>nothing</b>"

                }

            }
        }
    }


    function gen_obj(ttx, key) {
        const robj = {ttl: 0, size: ttx[key]}
        for (let v of ttx[key]) {
            robj.ttl += v
        }
        return robj
    }

    function add_table() {
        const expodb = GM_getValue("expo_counts", {
            "res": [0, 0, 0],
            "pirates": [0, 0, 0],
            "aliens": [0, 0, 0],
            "speed": [0, 0],
            "hole": [0],
            "ships": [0, 0, 0],
            "nothing": [0]
        })
        const ship_res_db = GM_getValue("expo_ship_res", {"res": [0, 0, 0], "fleet": {}})


        const res = gen_obj(expodb, "res")
        res.met = ship_res_db.res[0]
        res.krist = ship_res_db.res[1]
        res.deut = ship_res_db.res[2]
        const pirate = gen_obj(expodb, "pirates")
        const alien = gen_obj(expodb, "aliens")
        const ships = gen_obj(expodb, "ships")
        const speed = gen_obj(expodb, "speed")
        const hole = gen_obj(expodb, "hole")
        const nothing = gen_obj(expodb, "nothing")
        let ttl = nothing.ttl + hole.ttl + speed.ttl + ships.ttl + alien.ttl + pirate.ttl + res.ttl
        let txtr = `
           <table>
        <tr>
          <th>Event</th>
          <th>Total ${ttl}</th>
          <th>Percent</th>
        </tr>
          <tr>
          <td>Res<br>s:${res.size[0]} <br>m:${res.size[1]}<br>b:${res.size[2]}</td>
          <td>${res.ttl}<br>met:${shortly_number(res.met)}<br>krist:${shortly_number(res.krist)}<br>deut:${shortly_number(res.deut)}</td>
          <td>${Math.round(res.ttl * 1000 / ttl) / 10}<br>met:${shortly_number(res.met / (res.ttl + 0.00001))}<br>krist:${shortly_number(res.krist / (res.ttl + 0.00001))}<br>deut:${shortly_number(res.deut / (res.ttl + 0.00001))}</td>
        </tr>
         <tr>
          <td>Ships <br>s:${ships.size[0]} <br>m:${ships.size[1]}<br>b:${ships.size[2]}</td>
          <td>${ships.ttl}</td>
          <td>${Math.round(ships.ttl * 1000 / ttl) / 10}</td>
        </tr>

        <tr>
          <td>Pirates <br>s:${pirate.size[0]} <br>m:${pirate.size[1]}<br>b:${pirate.size[2]}</td>
          <td>${pirate.ttl}</td>
          <td>${Math.round(pirate.ttl * 1000 / ttl) / 10}</td>
        </tr>
        <tr>
          <td>Aliens <br>s:${alien.size[0]} <br>m:${alien.size[1]}<br>b:${alien.size[2]}</td>
          <td>${alien.ttl}</td>
          <td>${Math.round(alien.ttl * 1000 / ttl) / 10}</td>
        </tr>
        <tr>
          <td>speed <br>fast:${speed.size[0]} <br>slow:${speed.size[1]}</td>
          <td>${speed.ttl}</td>
          <td>${Math.round(speed.ttl * 1000 / ttl) / 10}</td>
        </tr>
        <tr>
          <td>nixname</td>
          <td>${hole.ttl}</td>
          <td>${Math.round(hole.ttl * 1000 / ttl) / 10}</td>
        </tr>
          <td>nichts</td>
          <td>${nothing.ttl}</td>
          <td>${Math.round(nothing.ttl * 1000 / ttl) / 10}</td>
        </tr>

      </table>
    `

        let expo_ships = ship_res_db.fleet
        let stable = `
                  <table>
        <tr>
          <th>shiptype</th>
          <th>Amount</th>
        </tr>`
        let snames = ["Spionagesonde", "Kleiner Transporter", "Großer Transporter", "Leichter Jäger", "Schwerer Jäger", "Kreuzer", "Schlachtschiff", "Schlachtkreuzer", "Bomber", "Zerstörer", "Spy Probe", "Light Cargo", "Heavy Cargo", "Light Fighter", "Heavy Fighter", "Cruiser", "Battleship", "Battle Cruiser", "Planet Bomber", "Star Fighter"]
        for (let stype of snames) {
            if (!(stype in expo_ships)) {
                continue
            }
            stable += `
                        <tr>
          <td>${stype}</td>
          <td>${expo_ships[stype]}</td>
        </tr>
            `
        }
        stable += "    </table>"

        return txtr + stable

    }


    // Fleet window
    function show_phalanx_exact() {
        if (!window.location.href.includes("page=phalanx")) {
            return
        }

        const mh = document.getElementsByClassName("fleets")
        for (let flight of mh) {
            let nd = new Date(parseInt(flight.getAttribute("data-fleet-end-time")) * 1000);
            flight.parentNode.innerHTML = '<span style="color:yellow">' + nd.getHours() + ":" + pad(nd.getMinutes(), 2) + ":" + pad(nd.getSeconds(), 2) + ' </span>| ' + flight.parentNode.innerHTML
        }


    }


    function showexact_time() {
        if (!window.location.href.includes("page=overview")) {
            return
        }

        const mh = document.getElementById("hidden-div2").getElementsByTagName("li")
        for (let flight of mh) {
            let nd = new Date(parseInt(flight.firstElementChild.getAttribute("data-fleet-end-time")) * 1000);
            flight.innerHTML = '<span style="color:yellow">' + nd.getHours() + ":" + pad(nd.getMinutes(), 2) + ":" + pad(nd.getSeconds(), 2) + ' </span>| ' + flight.innerHTML
        }


    }

    function set_arrival_time() {
        if (window.GetDuration && document.getElementsByName("speed").length) {


            let xf = document.getElementsByClassName("table519")


            xf[xf.length - 1].getElementsByTagName("input")[0].addEventListener("click", fleet_set_target);
        }
    }

    function fleet_set_target() {
        GM_setValue("fleet_arrival", dataFlyTime);
        const t_koords = document.getElementById("galaxy").value + ":" + document.getElementById("system").value + ":" + document.getElementById("planet").value

        GM_setValue("fleet_t_coords", [document.getElementById("planet").value == "16", t_koords]);
    }


    function get_expos_on_koord(koord) {

        const expos = GM_getValue("fleet_expos", {})
        if (!(koord in expos)) {
            return 0;
        }
        let nary = []
        const ctime = Date.now()
        for (let time of expos[koord]) {
            if (ctime - time < 86400000) {
                nary.push(time)
            }

        }
        expos[koord] = nary
        GM_setValue("fleet_expos", expos);
        return nary.length

    }

    function fleet_Expo_add() {
        const expos = GM_getValue("fleet_expos", {})
        const target = GM_getValue("fleet_t_coords", [false, "-1"])[1]
        if (!(target in expos)) {
            expos[target] = [];
        }
        expos[target].push(Date.now())
        GM_setValue("fleet_expos", expos);
    }

    function show_arrival() {
        if (!window.location.href.includes("page=fleetStep2")) {
            return;
        }


        let tbl = document.getElementsByClassName("table519")[0]
        tbl.getElementsByTagName("table")[1].insertRow(6)
        tbl.getElementsByTagName("table")[1].getElementsByTagName("tr")[6].innerHTML = '<td class="transparent" colspan="3" id="arr_time"></td>'
        const target = GM_getValue("fleet_t_coords", [false, "-1"])
        tbl.getElementsByTagName("table")[1].getElementsByTagName("td")[11].innerHTML += '<br> <a href="#" id="save_Res">Ausgewählte Rohstoffe Laden</a>'
        document.getElementById("save_Res").addEventListener("click", fleet_insert_res);

        if (target[0]) {
            let expoamt = get_expos_on_koord(target[1])
            tbl.getElementsByTagName("table")[1].insertRow(7)
            tbl.getElementsByTagName("table")[1].getElementsByTagName("tr")[7].innerHTML = '<td class="transparent" colspan="3">Expos:' + expoamt + '</td>'
            document.getElementsByClassName("table519")[0].getElementsByTagName("input")[document.getElementsByClassName("table519")[0].getElementsByTagName("input").length - 1].addEventListener("click", fleet_Expo_add);


        }
        window.dataFlyTime = GM_getValue("fleet_arrival", -1)
        setInterval(update_arrival, 1000)
        update_arrival();

    }

    function fleet_insert_res() {

        let f = GM_getValue("fleet_trans_res", [0, 0, 0]);
        document.getElementsByName("metal")[0].value = f[0]
        document.getElementsByName("crystal")[0].value = f[1]
        document.getElementsByName("deuterium")[0].value = f[2]
        calculateTransportCapacity();
    }

    function pad(num, size) {
        num = num.toString();
        while (num.length < size) num = "0" + num;
        return num;
    }

    function update_arrival() {
        const nd = new Date(new Date(serverTime).getTime() + window.dataFlyTime * 1000)
        document.getElementById("arr_time").innerText = "Arrival:   " + nd.getHours() + ":" + pad(nd.getMinutes(), 2) + ":" + pad(nd.getSeconds(), 2)
    }


    // res calculator


    function update_current_res() {
        const planet_res = GM_getValue("res", {})
        let coords = get_current_planet_id()

        planet_res[coords] = {
            met: parseInt(document.getElementById("current_metal").innerText.replace(/\./g, "")),
            krist: parseInt(document.getElementById("current_crystal").innerText.replace(/\./g, "")),
            deut: parseInt(document.getElementById("current_deuterium").innerText.replace(/\./g, "")),
            met_max: parseInt(document.getElementById("max_metal").innerText.replace(/\./g, "")),
            krist_max: parseInt(document.getElementById("max_crystal").innerText.replace(/\./g, "")),
            deut_max: parseInt(document.getElementById("max_deuterium").innerText.replace(/\./g, "")),
            energy: parseInt(("" + document.getElementById("resources_mobile").childNodes[7].innerText.split("/")[0].split("\n")[1]).replaceAll(".", "")),
            energy_max: 99999999,
            time: Date.now()
        }
        GM_setValue("res", planet_res)
    }

    function calctime() {

        let met_needed = Math.max(Number(document.getElementById("t_met").value) - Number(document.getElementById("c_met").innerText), 0)
        let krist_needed = Math.max(Number(document.getElementById("t_krist").value) - Number(document.getElementById("c_krist").innerText), 0)
        let deut_needed = Math.max(Number(document.getElementById("t_deut").value) - Number(document.getElementById("c_deut").innerText), 0)

        let met_time = 100 * met_needed / Number(document.getElementById("p_met").innerText)
        let krist_time = 100 * krist_needed / Number(document.getElementById("p_krist").innerText)
        let deut_time = 100 * deut_needed / Number(document.getElementById("p_deut").innerText)
        document.getElementById("time_til_got").innerText = Math.round(Math.max(met_time, krist_time, deut_time)) / 100 + " h";
    }

    function set_planets() {
        const planets_selected = GM_getValue("select", {})
        let planets = document.getElementById("planetSelector")
        for (let p of planets.children) {
            let pname = p.innerText.split("[")[1]
            if (p.innerText.includes("(")) {
                pname = pname + "M"
            }
            if (!(pname in planets_selected)) {
                planets_selected[pname] = 0;
            }
        }
        GM_setValue("select", planets_selected)
    }


    function show_prodph() {
        const planet_production = GM_getValue("prod", {})
        let met = 0
        let krist = 0
        let deut = 0
        let met_t = 0
        let krist_t = 0
        let deut_t = 0
        const planet_res = GM_getValue("res", {})
        for (let planet in planet_production) {
            if (!document.getElementById(planet).checked) {
                continue
            }

            met += planet_production[planet].met
            krist += planet_production[planet].krist
            deut += planet_production[planet].deut

        }
        for (let planet in planet_res) {
            if (!document.getElementById(planet).checked) {
                continue
            }
            met_t += planet_res[planet].met
            krist_t += planet_res[planet].krist
            deut_t += planet_res[planet].deut

        }

        document.getElementById("p_met").innerText = met;
        document.getElementById("p_krist").innerText = krist;
        document.getElementById("p_deut").innerText = deut;

        document.getElementById("met_top_p").innerHTML = "+" + shortly_number(met);
        document.getElementById("krist_top_p").innerHTML = "+" + shortly_number(krist);
        document.getElementById("deut_top_p").innerHTML = "+" + shortly_number(deut);


        document.getElementById("c_met").innerText = met_t;
        document.getElementById("c_krist").innerText = krist_t;
        document.getElementById("c_deut").innerText = deut_t;


    }


    function update_production() {
        if (document.getElementsByTagName("form").length == 1) {
            if (document.getElementsByTagName("form")[0].action.includes("page=resources")) {
                let f1 = document.getElementsByTagName("form")[0]
                let resperhour = f1.getElementsByTagName("tr")[f1.getElementsByTagName("tr").length - 3].innerText.replace(/\./g, "").split("\t")
                let coords = get_current_planet_id()
                const planet_production = GM_getValue("prod", {})
                planet_production[coords] = {
                    met: parseInt(resperhour[1]),
                    krist: parseInt(resperhour[2]),
                    deut: parseInt(resperhour[3])
                }
                GM_setValue("prod", planet_production)

            }
        }
    }

    function saveres() {
        GM_setValue("res_input", [document.getElementById("t_met").value, document.getElementById("t_krist").value, document.getElementById("t_deut").value])
        calctime();
    }


    // main add

    function gen_fieldset(id, func, name) {

        const fields = GM_getValue("menu_vis", {
            "expo_menu": "none",
            "custom_fleet_menu": "none",
            "custom_commander_fleet": "none"
        })
        return `<fieldset ><legend style="float:right"><a  id="${id}" href="#" onclick="var spoilernode = this.parentNode.parentNode.lastChild; if (spoilernode.style.display == 'block') spoilernode.style.display='none'; else spoilernode.style.display = 'block'; return false;">${name}</a></legend><div style="display: ${fields[id]}" class="tscroll">${func()}</div></fieldset>`


    }

    function addstuff() {
        const planets_selected = GM_getValue("select", {})
        var psel_str = ""


        for (let p of get_sortet_planet_list(Object.keys(planets_selected))) {
            let checked = ""
            if (planets_selected[p] == 1) {
                checked = "checked"
            }
            psel_str += '<input type="checkbox" id="' + p + '"  ' + checked + ' >[' + p + '<span id="full_' + p + '"></span><br>'


        }

        var div = document.createElement("div");
        div.style.cssText = ' grid-row: 2; grid-column: 1;    overflow-x: hidden;width: 20vw;'
        var ct = document.createElement("center");
        ct.id = "atain"
        div.className = "no-mobile";
        div.appendChild(ct)
        document.getElementsByClassName("wrapper")[0].appendChild(div)
        var li = document.createElement("li")
        li.style.cssText = "    overflow: hidden;    font-size: 12px;    background: #2a2e31;"
        li.id = "disc2"
        li.innerHTML = `

M:<input placeholder="met" id="t_met" type="number" step="1000" ><br>
K:<input placeholder="krist" id="t_krist" type="number" step="1000"><br>
D:<input placeholder="deut" id="t_deut" type="number" step="1000">
<br>
current:<br>M:
<span id="c_met"></span><br>K:<span id="c_krist"></span><br>D:<span id="c_deut"></span>
<br>
prod:
<span id="p_met"></span>,<span id="p_krist"></span>,<span id="p_deut"></span>
<br>
time:
<span id="time_til_got"></span>
<br>

` + psel_str

        document.getElementById("menu").appendChild(li)
        document.getElementById("atain").innerHTML = `
${gen_fieldset("custom_commander_res", commander_res_table, "Commander Res")}
${gen_fieldset("custom_commander_building", commander_building_table, "Commander Build")}
${gen_fieldset("custom_commander_fleet", commander_fleet_table_invert, "Commander Fleet")}
${gen_fieldset("custom_commander_def", commander_def_table, "Commander Def")}
${gen_fieldset("expo_menu", add_table, "Expo statistik")}
${gen_fieldset("custom_fleet_menu", set_custom_fleet, "Expo custom fleet")}
${gen_fieldset("priotable", priotable, "Fleet prio")}


        `
        priotable_interactive();
        document.getElementById("t_met").addEventListener("change", saveres);
        document.getElementById("t_krist").addEventListener("change", saveres);
        document.getElementById("t_deut").addEventListener("change", saveres);
        document.getElementById("expo_menu").addEventListener("click", change_menu);
        document.getElementById("custom_fleet_menu").addEventListener("click", change_menu);
        document.getElementById("custom_commander_fleet").addEventListener("click", change_menu);
        document.getElementById("custom_commander_def").addEventListener("click", change_menu);
        document.getElementById("custom_commander_building").addEventListener("click", change_menu);
        document.getElementById("custom_commander_res").addEventListener("click", change_menu);

        document.getElementById("priotable").addEventListener("click", change_menu);



        const resstatus = GM_getValue("res_input", [0, 0, 0])
        document.getElementById("t_met").value = resstatus[0]
        document.getElementById("t_krist").value = resstatus[1]
        document.getElementById("t_deut").value = resstatus[2]
        let a = document.createElement("div")
        let b = document.createElement("div")
        let c = document.createElement("div")
        a.style.cssText = 'font-size: 11px;color:#0F0'
        b.style.cssText = 'font-size: 11px;color:0F0'
        c.style.cssText = 'font-size: 11px;color:0F0'
        a.innerHTML = "<span style='color:yellow' id='met_top'></span> <span id='met_top_p' style='color:#0F0'></span>"
        b.innerHTML = "<span style='color:yellow' id='krist_top'></span> <span id='krist_top_p' style='color:#0F0'></span>"
        c.innerHTML = "<span style='color:yellow' id='deut_top'></span> <span id='deut_top_p' style='color:#0F0'></span>"

        document.getElementsByClassName("no-mobile")[4].appendChild(a)
        document.getElementsByClassName("no-mobile")[6].appendChild(b)
        document.getElementsByClassName("no-mobile")[8].appendChild(c)
        for (let n of document.getElementById("disc2").querySelectorAll('input[type="checkbox"]')) {
            n.addEventListener('change', planet_select_change);

        }
    }

    function change_menu() {
        const cm = GM_getValue("menu_vis", {})
        let id = this.id
        if (cm[id] == "none") {
            cm[id] = "block"

        } else {
            cm[id] = "none"
        }

        GM_setValue("menu_vis", cm);
    }


    function planet_select_change() {
        const planets_selected = GM_getValue("select", {})
        planets_selected[this.id] = Math.abs(planets_selected[this.id] - 1)
        GM_setValue("select", planets_selected)
        show_prodph();
        calctime();
    }

    function show_exp_value() {
        if (!window.location.href.includes("pr0game.com/game.php?page=fleetTable")) {
            return
        }
        if (document.getElementsByClassName("table519")[0].getElementsByTagName("tr")[0].innerText.includes(":") == false) {
            return;
        }

        let fleets = document.getElementsByClassName("table519")[0]
        let f_rows = fleets.getElementsByTagName("tr")
        let fpoints = 0

        for (let i = 2; i < f_rows.length; i++) {
            let tds = f_rows[i].getElementsByTagName("td")
            if (tds.length != 4) {
                continue
            }
            let name = tds[0].innerText.trim()
            if (name == "Solarsatellit" || name == "Solar Satellite") {
                continue
            }
            let setamt = Number(tds[3].firstChild.value.replaceAll(".", ""))
            fpoints += exp_Values[name] * setamt
        }
        fleets.getElementsByTagName("td")[3].innerText = fpoints + " / " + GM_getValue("exp_max", -1)
    }


    function get_current_values() {
        const planet_production = GM_getValue("prod", {})
        const planet_res = GM_getValue("res", {})

        let ctime = Date.now()
        const live_perp = {}
        for (let p in planet_production) {
            live_perp[p] = {"met": -1, "krist": -1, "deut": -1, energy: planet_res[p].energy}
            const hours = (ctime - planet_res[p].time) / 3600000;
            const res_current = {}
            const fulltimes = {}
            for (let res of ["met", "krist", "deut"]) {
                let prod = hours * planet_production[p][res]
                let lres = planet_res[p][res]
                let current_res = Math.floor(Math.max(lres, Math.min(planet_res[p][res + "_max"], lres + prod)))

                let fulltime = Math.floor(Math.max(0, (planet_res[p][res + "_max"] - current_res) / planet_production[p][res]))
                res_current[res] = current_res
                fulltimes[res] = fulltime
                live_perp[p][res] = current_res

            }
            let minfulltime = Math.min(...Object.values(fulltimes))
            let min_res = "gold"
            for (let res in fulltimes) {
                if (minfulltime == fulltimes[res]) {
                    min_res = res
                }

            }
            live_perp[p].ftime = minfulltime
            live_perp[p].fres = min_res

        }
        return live_perp

    }

    function calcfull() {

        const planets_selected = GM_getValue("select", {})
        const live_res = {met: 0, krist: 0, deut: 0}
        const planet_live = get_current_values()
        for (let p in planet_live) {
            for (let res of ["met", "krist", "deut"]) {
                if (planets_selected[p] == 1) {
                    live_res[res] += planet_live[p][res]
                }
            }
            document.getElementById("full_" + p).innerText = "  " + planet_live[p].ftime + " h " + planet_live[p].fres;
        }
        document.getElementById("c_met").innerText = live_res.met
        document.getElementById("c_krist").innerText = live_res.krist
        document.getElementById("c_deut").innerText = live_res.deut
        document.getElementById("met_top").innerHTML = shortly_number(live_res.met)
        document.getElementById("krist_top").innerHTML = shortly_number(live_res.krist)
        document.getElementById("deut_top").innerHTML = shortly_number(live_res.deut)
    }

    function get_best_ship_type() {
        let bestname = ""
        let bestscore = 0;
        let bestid = -1
        let fleets = document.getElementsByClassName("table519")[0]
        let f_rows = fleets.getElementsByTagName("tr")
        for (let i = 2; i < f_rows.length; i++) {
            let tds = f_rows[i].getElementsByTagName("td")
            if (tds.length != 4) {
                continue
            }
            let name = tds[0].innerText.trim()
            let idx = battleships.indexOf(name)
            if (idx > bestid) {
                bestname = name
                bestscore = exp_Values[name]
                bestid = idx
            }


        }

        return [bestname, bestscore]
    }


    function fleet_expo() {
        if (!window.location.href.includes("pr0game.com/game.php?page=fleetTable")) {
            return
        }
        let beststuff = get_best_ship_type()
        let fleets = document.getElementsByClassName("table519")[0]
        let f_rows = fleets.getElementsByTagName("tr")
        let gts_needed = Math.ceil((GM_getValue("exp_max", -1) - beststuff[1]) / 60)

        for (let i = 2; i < f_rows.length; i++) {
            let tds = f_rows[i].getElementsByTagName("td")
            if (tds.length != 4) {
                continue
            }
            let name = tds[0].innerText.trim()
            if (name == "Spionagesonde" || name == "Spy Probe") {
                tds[3].firstChild.value = 1
                continue
            }
            if (name == beststuff[0]) {
                tds[3].firstChild.value = 1
                continue

            }


            if (name != "Großer Transporter" && name != "Heavy Cargo") {
                tds[3].firstChild.value = 0
                continue
            }
            let amt = Number(tds[1].innerText.replaceAll(".", ""))
            if (amt < gts_needed) {
                alert("Not at expo cap!")
            }
            tds[3].firstChild.value = Math.min(amt, gts_needed)

        }
        show_exp_value();
    }


    function custom_expo() {
        console.log();
        if (!window.location.href.includes("pr0game.com/game.php?page=fleetTable")) {
            return
        }
        noShips();
        const custom_fleet = GM_getValue("custom_fleets", {})[this.getAttribute("cfleet")]
        let warn=false;
        for (let stype in custom_fleet) {
            let obj = document.getElementById(`ship${stype}_input`)
            if (obj) {
                obj.value = custom_fleet[stype]

            }else{
                if(custom_fleet[stype]!="0"){
                    warn=true;
                }
            }
        }
        show_exp_value();
        if(warn){
            alert("not all ships could be selected!")
        }
    }

    function gt_expo() {
        if (!window.location.href.includes("pr0game.com/game.php?page=fleetTable")) {
            return
        }

        let fleets = document.getElementsByClassName("table519")[0]
        let f_rows = fleets.getElementsByTagName("tr")
        let gts_needed = Math.ceil(GM_getValue("exp_max", -1) / 60)

        for (let i = 2; i < f_rows.length; i++) {

            let tds = f_rows[i].getElementsByTagName("td")
            if (tds.length != 4) {
                continue
            }
            let name = tds[0].innerText.trim()
            if (name == "Spionagesonde" || name == "Spy Probe") {
                tds[3].firstChild.value = 1
                continue
            }

            if (name != "Großer Transporter" && name != "Heavy Cargo") {
                tds[3].firstChild.value = 0
                continue
            }
            let amt = Number(tds[1].innerText.replaceAll(".", ""))
            tds[3].firstChild.value = Math.min(amt, gts_needed)
            if (amt < gts_needed) {
                alert("Not at expo cap!")
            }

        }
        show_exp_value();
    }


    function fleet_showcustom(){
        let cfleet = GM_getValue("custom_fleets", {})
        let x=""
        for(let k in cfleet){
            x += '   <a cfleet="'+k+'" class="cfleet" href="javascript:;">'+k+'</a>'

        }
        return x;
    }
    function add_expo_buttons() {
        if (!window.location.href.includes("pr0game.com/game.php?page=fleetTable")) {
            return
        }
        if (document.getElementsByClassName("table519")[0].getElementsByTagName("tr")[0].innerText.includes(":") == false) {
            return;
        }

        let fleets = document.getElementsByClassName("table519")[0]

        let f_rows = fleets.getElementsByTagName("tr")

        let ex = fleet_showcustom()
        let row = document.createElement("tr");


        row.innerHTML += `<td colspan="4"><center>
<a id="gt_expo" href="javascript:;">GT_expo</a>
<a id="fleet_expo" href="javascript:;">Fleet_expo</a>
` + ex + `</center></td> `
        fleets.childNodes[1].insertBefore(row, f_rows[f_rows.length - 1]);
        document.getElementById("gt_expo").addEventListener("click", gt_expo);
        document.getElementById("fleet_expo").addEventListener("click", fleet_expo);
        for(let node of document.querySelectorAll(".cfleet")){

            node.addEventListener("click", custom_expo);
        }
        let sdiv = `<center id="res_sender">Met:<input type="number" value="${document.getElementById("current_metal").innerText.replace(/\./g, "")}"> Krist:<input type="number" value="${document.getElementById("current_crystal").innerText.replace(/\./g, "")}"> Deut:<input type="number" value="${document.getElementById("current_deuterium").innerText.replace(/\./g, "")}"> <br><button id="gt_send" dt="ship203_input">gt (42)</button> <button id="kt_send" dt="ship202_input">kt (132)</button><br></center>`

        let x = document.createElement("div");
        x.innerHTML = sdiv
        document.getElementsByTagName("content")[0].insertBefore(x, document.getElementsByTagName("content")[0].childNodes[6])
        for (let ipt of x.getElementsByTagName("input")) {

            ipt.addEventListener('change', set_tranport_buttons);
        }
        for (let ipt of x.getElementsByTagName("button")) {

            ipt.addEventListener('click', set_transport_amt);
        }
        set_tranport_buttons();
    }

    function set_tranport_buttons() {
        let ttl = 0
        for (let ipt of document.getElementById("res_sender").getElementsByTagName("input")) {
            ttl += parseInt(ipt.value)
        }

        let gts = Math.floor(ttl / 25000)
        let kts = Math.floor(ttl / 5000)
        if (ttl / 25000 - gts > 0.8) {
            gts += 2
        } else {
            gts += 1
        }
        if (ttl / 5000 - kts > 0.6) {
            kts += 2
        } else {
            kts += 1
        }
        document.getElementById("gt_send").innerText = "gt select (" + gts + ")"
        document.getElementById("kt_send").innerText = "kt select (" + kts + ")"
        document.getElementById("gt_send").setAttribute("data", gts)
        document.getElementById("kt_send").setAttribute("data", kts)
        document.getElementsByClassName("table519")[0].querySelector('input[type="submit"]')?.addEventListener('click', save_trans_res);
    }

    function set_transport_amt() {
        let amt = this.getAttribute("data")
        let stype = this.getAttribute("dt")
        noShips()
        document.getElementById(stype).value = amt;

    }

    function save_trans_res() {
        let inpts = document.getElementById("res_sender").getElementsByTagName("input")
        GM_setValue("fleet_trans_res", [inpts[0].value, inpts[1].value, inpts[2].value]);
    }

    function save_planet_fleet() {
        if (!window.location.href.includes("page=fleetTable")) {
            return
        }
        const fleet_overview = GM_getValue("commander_fleet", {})
        let coords = get_current_planet_id()
        const newfleet = {}
        let fleets = document.getElementsByClassName("table519")[0]
        let f_rows = fleets.getElementsByTagName("tr")
        for (let i = 2; i < f_rows.length; i++) {

            let tds = f_rows[i].getElementsByTagName("td")
            if (tds.length != 4 || tds[1].id == null) {

                continue
            }
            newfleet[tds[1].id.substring(4, 7)] = tds[1].innerText

        }
        fleet_overview[coords] = newfleet
        GM_setValue("commander_fleet", fleet_overview);
    }


    function remove_old_planets() {
        let planets = document.getElementById("planetSelector")
        if (planets == null) {
            return;
        }
        let pids = {}
        for (let p of planets.children) {
            let pname = p.innerText.split("[")[1]
            if (p.innerText.includes("(")) {
                pname = pname + "M"
            }
            pids[pname] = 1
        }
        const gmnames = ["commander_building",
            "res",
            "prod",
            "commander_fleet",
            "commander_def"]
        for (let gmv of gmnames) {
            let mvx = GM_getValue(gmv, {})
            let rmv = []
            for (let k in mvx) {
                if (pids[k] !== 1) {
                    rmv.push(k)
                }
            }
            for (let k of rmv) {
                delete mvx[k];
            }
            GM_setValue(gmv, mvx)
        }
    }

    remove_old_planets();


    function add_exp_eventlisteners() {
        if (!window.location.href.includes("pr0game.com/game.php?page=fleetTable")) {
            return
        }
        let fleets = document.getElementsByClassName("table519")[0]
        let f_rows = fleets.querySelectorAll("input")
        for (let i of f_rows) {
            i.addEventListener("change", () => setTimeout(show_exp_value, 100));
        }
        f_rows = fleets.querySelectorAll("a")
        for (let i of f_rows) {

            i.addEventListener("click", () => setTimeout(show_exp_value, 100));
        }
    }


    function set_max_exp_score() {
        let stats = document.getElementById("stats")
        if (!stats) {
            return
        }
        if (stats.getElementsByTagName("select")[1].value != "1") {
            return
        }
        if (document.getElementsByClassName("tooltip")[0].innerText != "1") {
            return
        }

        let bestscore = parseInt(document.getElementsByClassName("table519")[1].getElementsByTagName("td")[4].innerText.replace(/\./g, ""))
        let exposcore = 2400
        if (bestscore > 100000) {
            exposcore = 6000
        }
        if (bestscore > 1000000) {
            exposcore = 9000
        }
        if (bestscore > 5000000) {
            exposcore = 12000
        }
        if (bestscore > 25000000) {
            exposcore = 15000
        }
        if (bestscore > 50000000) {
            exposcore = 18000
        }

        if (bestscore > 75000000) {
            exposcore = 21000
        }
        if (bestscore > 100000000) {
            exposcore = 25000
        }

        GM_setValue("exp_max", exposcore)
    }


    const exp_Values = {
        "Kleiner Transporter": 20,
        "Großer Transporter": 60,
        "Leichter Jäger": 20,
        "Schwerer Jäger": 50,
        "Kreuzer": 135,
        "Schlachtschiff": 300,
        "Kolonieschiff": 150,
        "Recycler": 80,
        "Spionagesonde": 5,
        "Bomber": 375,
        "Zerstörer": 550,
        "Todesstern": 45000,
        "Schlachtkreuzer": 350,
        "Solarsatellit": 0,

        "Light Cargo": 20,
        "Heavy Cargo": 60,
        "Light Fighter": 20,
        "Heavy Fighter": 50,
        "Cruiser": 135,
        "Battleship": 300,
        "Colony Ship": 150,
        "Spy Probe": 5,
        "Planet Bomber": 375,
        "Star Fighter": 550,
        "Battle Fortress": 45000,
        "Battle Cruiser": 350,
        "Solar Satellite": 0

    }

    function add_talbe_scroll() {

        var style = document.createElement('style');
        style.type = 'text/css';
        if (style.styleSheet) {
            style.styleSheet.cssText = `.tscroll {
  width: 100%;
  overflow-x: auto;
}

.tscroll table *:first-child {
  position: sticky;
  left: 0;
}
`;
        } else {
            style.appendChild(document.createTextNode(`.tscroll {
    width: 20vw;
  overflow-x: auto;
}

.tscroll table *:first-child {
  position: sticky;
  left: 0;
}
`));
        }
        document.getElementsByTagName('head')[0].appendChild(style);


    }

    function set_importance_fleet() {
        if (!window.location.href.includes("page=fleetStep2")) {
            return
        }
        const priority = GM_getValue("fleet_prio_list", ["radio_2", "radio_6", "radio_1", "radio_4", "radio_3", "radio_5", "radio_17"])
        for (let prio of priority) {
            let btn = document.getElementById(prio)
            if (btn) {
                if (btn.checked) {
                    return
                }
            }
        }

        for (let prio of priority) {
            let btn = document.getElementById(prio)
            if (btn) {
                btn.checked = true
                break
            }

        }
    }

    function priotable() {
        const priority = GM_getValue("fleet_prio_list", ["radio_2", "radio_6", "radio_1", "radio_4", "radio_3", "radio_5", "radio_17"])
        const btn_to_action = {
            "radio_2": "AKS-angriff",
            "radio_3": "transport",
            "radio_6": "Spio",
            "radio_17": "transfer",
            "radio_1": "angreifen",
            "radio_5": "halten",
            "radio_4": "stationieren"
        }

        var html = "<table id='event_prio_list'>       <tr>   <th>Event</th><th>Priority</th>       </tr>"
        for (let action in priority) {
            html += `<tr id="${priority[action]}" draggable="true" ondragstart="priodrag()"  ondragover="dragover()"> <td >${btn_to_action[priority[action]]}</td><td><input id="${priority[action]}" style="    width: 50px;" value="${action}"></td></tr> `
        }

        html += "</table>"
        return html


    }


    function gen_prio_list() {
        let inpts = document.getElementById("event_prio_list").querySelectorAll("input")
        let aryobj = []
        for (let event of inpts) {
            aryobj.push({id: event.id, prio: event.value})

        }

        aryobj.sort(function (a, b) {
            return a.prio - b.prio
        });
        let nprio = []
        for (let v of aryobj) {
            nprio.push(v.id)
        }
        GM_setValue("fleet_prio_list", nprio);
    }

    function priotable_interactive() {
        let inpts = document.getElementById("event_prio_list").querySelectorAll("input")
        for (let ele of inpts) {
            ele.addEventListener("change", gen_prio_list);

        }


    }

    window.row;
    var row = event.target;

    window.priodrag = function () {
        row = event.target;
    }
    window.dragover = function () {
        var e = event;
        e.preventDefault();

        let children = Array.from(e.target.parentNode.parentNode.children);

        if (children.indexOf(e.target.parentNode) > children.indexOf(row))
            e.target.parentNode.after(row);
        else
            e.target.parentNode.before(row);
        gen_prio_list_move();
    }

    function gen_prio_list_move() {

        let inpts = document.getElementById("event_prio_list").querySelectorAll("tr")
        let aryobj = []
        for (let event of inpts) {
            if (event.id == "") {
                continue
            }
            aryobj.push(event.id)

        }
        GM_setValue("fleet_prio_list", aryobj);


    }






    function loadcustomfleets() {
        let cfleet = GM_getValue("custom_fleets", {})
        for (let cf in cfleet) {
            showfleet(cf)
            break
        }
        for (let cf in cfleet) {
            let obj = document.createElement("option");
            obj.value = cf;
            obj.innerText = cf;
            document.getElementById("cfleet_select").appendChild(obj);
        }

        document.getElementById("cfleet_select").addEventListener("change", change_fleet_select);
        document.getElementById("cf_save").addEventListener("click", save_fleet_Select);
        document.getElementById("cf_del").addEventListener("click", cf_remove);
        for (let f in exp_values) {
            document.getElementById("ship_" + f).addEventListener("change", showexpopoints);

        }
        showexpopoints();

    }

    function save_fleet_Select() {
        let obj = {}
        let cfleet = GM_getValue("custom_fleets", {})
        for (let shipid of ["202",
            "203",
            "204",
            "205",
            "206",
            "207",
            "210",
            "211",
            "213",
            "215",
            "214",
            "209",
            "208"
        ]) {
            obj[shipid] = document.getElementById("ship_" + shipid).value
        }
        let fname = document.getElementById("cfleet_name").value
        if (!(fname in cfleet)) {
            let obj = document.createElement("option");
            obj.value = fname;
            obj.innerText = fname;
            document.getElementById("cfleet_select").appendChild(obj);
        }

        cfleet[fname] = obj
        GM_setValue("custom_fleets", cfleet)
    }

    function change_fleet_select() {

        showfleet(document.getElementById("cfleet_select").value);
    }

    function cf_remove() {
        let cfleet = GM_getValue("custom_fleets", {})
        let oname = document.getElementById("cfleet_name").value
        let rv = confirm("Remove fleet template " + oname + " ?")

        if (rv == false) {
            return;
        }

        document.getElementById("cfleet_name").value = "";
        let rmv = document.querySelector("#cfleet_select option[value='" + oname + "']")
        if (rmv) {
            rmv.remove()
            delete cfleet[oname];
            showfleet(document.getElementById("cfleet_select").value)
        }

        GM_setValue("custom_fleets", cfleet)
    }


    function showfleet(name) {
        let cfleet = GM_getValue("custom_fleets", {})
        document.getElementById("cfleet_name").value = name;
        for (let sid in cfleet[name]) {
            document.getElementById("ship_" + sid).value = cfleet[name][sid]

        }
    }

    function showexpopoints() {
        let fleet = {}
        for (let f in exp_values) {

            fleet[f] = document.getElementById("ship_" + f).value

        }

        document.getElementById("ship_expo_points").innerText = calcexpopoints(fleet)



    }


    function calcexpopoints(fleet) {
        let points = 0;
        for (let f in fleet) {
            if (exp_values[f] === null) {
                continue;
            }
            points += exp_values[f] * parseInt(fleet[f])

        }
        return points;



    }


    const exp_values = {
        "202": 20,
        "203": 60,
        "204": 20,
        "205": 50,
        "206": 135,
        "207": 300,
        "208": 150,
        "209": 80,
        "210": 5,
        "211": 375,
        "213": 550,
        "214": 45000,
        "215": 350
    }
    show_phalanx_exact()
    set_importance_fleet();
    get_imperiom_stuff();
    marketstuff();

    add_talbe_scroll();
    const battleships = ["Leichter Jäger", "Schwerer Jäger", "Kreuzer", "Schlachtschiff", "Schlachtkreuzer", "Bomber", "Zerstörer", "Light Fighter", "Heavy Fighter", "Cruiser", "Battleship", "Battle Cruiser", "Planet Bomber", "Star Fighter"]
    commander_def_reader();
    commander_building_reader();

    save_planet_fleet();
    selectmesages();
    add_expo_buttons();
    add_exp_eventlisteners();
    show_exp_value();
    set_max_exp_score();
    set_planets();

    update_current_res();
    update_production();
    addstuff();
    show_prodph();
    calcfull();

    calctime();
    set_arrival_time();
    show_arrival();
    loadcustomfleets();
    showexact_time();
    setInterval(calcfull, 1000);


})();